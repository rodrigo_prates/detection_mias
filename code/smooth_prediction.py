from SegImages import SegImages
import os
import config
from keras_segmentation.predict import model_from_checkpoint_path

seg = SegImages()

#imagePath = 'predseg_3-RJS-739A-RJ_5.476-40-5.479-10_FotoHDI_T1_CX37-39_8x.png'
#origImage = '3-RJS-739A-RJ_5.476-40-5.479-10_FotoHDI_T1_CX37-39_res10.png'
#winSize = 224
#stepsize = 224
#imageOutPath = 'smoothed_' + str(winSize) + '_' + str(stepsize) + '_' + str(thres) + '_' + imagePath
#seg.smoothPredictedImage(imagePath, imageOutPath, origImage, winSize=winSize, stepsize=stepsize, thres=thres)
#Melhores resultados: 224,224,0.85 e 224,112,0.9

#origImage = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_PNG\\MIAS\\kfolds\\test0_input\\mdb263.png"
#thres = 0.7
#modelpath = os.path.sep.join([config.OUTPUTPATH, 'checkpoints', 'mobilenet_unet_none_win224_metric_accuracy_mias'])
#model = model_from_checkpoint_path(modelpath, True)
#smooth = '1'
#outputpath = os.path.sep.join([config.DIRPATH, 'PREDICTIONS'])
#seg.paint_prediction(origImage, model, config.NEWLABELS, outputpath, origsize=True, smooth=smooth, thres=thres)

origImage = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\DDSM_FULL_VIEW_PNG\\images\\P_00001_LEFT_CC_000000.dcm.png"
modelpath = os.path.sep.join([config.OUTPUTPATH, 'checkpoints', 'mobilenet_unet_none_win224_metric_accuracy_mias.h5'])
model = model_from_checkpoint_path(modelpath, True)
smooth = '0'
norm_pos='left'
outputpath = os.path.sep.join([config.DIRPATH, 'PREDICTIONS'])
seg.paint_prediction(origImage, model, config.NEWLABELS, outputpath, origsize=True, smooth=smooth, norm_pos=norm_pos)