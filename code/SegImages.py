import csv
from csv import writer
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import shutil
import cv2
import math
import PIL
import random

from sklearn.metrics import confusion_matrix, jaccard_score, precision_score, recall_score, accuracy_score, f1_score, roc_auc_score
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import VGG16
from keras.layers.core import Dropout
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers import Input
from keras.models import Model
from keras.optimizers import SGD
import keras.backend as K

from datetime import datetime

from sklearn.model_selection import train_test_split, KFold

from keras_segmentation.models.unet import vgg_unet, resnet50_unet, mobilenet_unet, unet
from keras_segmentation.models.segnet import vgg_segnet, resnet50_segnet, mobilenet_segnet, segnet
from keras_segmentation.models.fcn import fcn_8, fcn_32
from keras_segmentation.predict import model_from_checkpoint_path
from keras_segmentation.data_utils.data_loader import *
from keras_segmentation.models.config import IMAGE_ORDERING
from keras_segmentation import metrics

from matplotlib.ticker import MultipleLocator

import gc
from scipy.stats import ttest_ind, f_oneway
import pandas as pd
import itertools
import pydicom
from shapely.geometry import Polygon
import json
import config

from imgaug import augmenters as iaa
import imageio
import time
from pathlib import Path
from matplotlib import ticker

from tqdm import tqdm
from colorama import Fore
import base64
import ast
from statsmodels.stats.multicomp import pairwise_tukeyhsd

import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
#tf.config.experimental.set_per_process_memory_fraction(0.75)
tf.config.experimental.set_memory_growth(gpus[0], True)

#from keras.backend.tensorflow_backend import set_session
#from keras.backend.tensorflow_backend import clear_session
#from keras.backend.tensorflow_backend import get_session


class SegImages:

    SEGMODELS = {
      "unet": unet,
      "vgg_unet": vgg_unet,
      "resnet50_unet": resnet50_unet,
      "mobilenet_unet": mobilenet_unet,
      "segnet": segnet,
      "vgg_segnet": vgg_segnet,
      "resnet50_segnet": resnet50_segnet,
      "mobilenet_segnet": mobilenet_segnet,
      "fcn_8": fcn_8,
      "fcn_32": fcn_32
    }

    TRAGETSIZE = {
      "unet": (256, 256),
      "vgg_unet": (224, 224),
      "resnet50_unet": (224, 224),
      "mobilenet_unet": (224, 224),
      "segnet":  (256, 256),
      "vgg_segnet": (224, 224),
      "resnet50_segnet": (224, 224),
      "mobilenet_segnet": (224, 224),
      "fcn_8": (224, 224),
      "fcn_32": (224, 224)
   }

    def __init__(self):
        pass

    def write_list_to_csv(self, csvdata, file):
      with open(file, 'a+', newline='') as csvfile:
          csv_writer = writer(csvfile)
          csv_writer.writerow(['fold', 'accuracy', 'std'])
          for elements in csvdata:
              csv_writer.writerow(elements)

    def split_data_from_folder(self, inputpath, labelpath, outputpath, trainsize=0.75):
        input_list = []
        label_list = []
        for file in sorted(os.listdir(inputpath)):
            input_list.append(file)
        for file in sorted(os.listdir(labelpath)):
            label_list.append(file)

        input_train, input_test, input_val, label_train, label_test, label_val = self.data_split(input_list, label_list, trainsize)
        self.organize_dataset('train', inputpath, labelpath, outputpath, input_train, label_train)
        self.organize_dataset('val', inputpath, labelpath, outputpath, input_val, label_val)
        self.organize_dataset('test', inputpath, labelpath, outputpath, input_test, label_test)

    def split_data_cross_validation(self, inputpath, labelpath, outputpath, trainsize=0.90, kfolds=4, norm_pos=None, is_mias=False, 
                                    do_augmentation=False, do_denoise=False):
        input_list = []
        label_list = []
        for file in sorted(os.listdir(inputpath)):
            input_list.append(file)
        for file in sorted(os.listdir(labelpath)):
            label_list.append(file)

        folds = KFold(n_splits=kfolds, shuffle=True, random_state=1)

        kfold_index=0
        for train_index, test_index in tqdm(folds.split(input_list), desc='Kfold index: '+str(kfold_index), 
                position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.BLUE, Fore.RESET)):
            input_sub_list = [ input_list[index] for index in train_index ]
            label_sub_list = [ label_list[index] for index in train_index ]
            #input_train, input_test, input_val, label_train, label_test, label_val = self.data_split(input_sub_list, label_sub_list, trainsize)
            test_size = 1 - trainsize
            input_train, input_val, label_train, label_val = train_test_split(input_sub_list, label_sub_list, test_size=test_size)
            input_test = [ input_list[index] for index in test_index ]
            label_test = [ label_list[index] for index in test_index ]
            self.organize_dataset('train'+str(kfold_index), inputpath, labelpath, outputpath, input_train, label_train, norm_pos, is_mias, do_augmentation, do_denoise)
            self.organize_dataset('val'+str(kfold_index), inputpath, labelpath, outputpath, input_val, label_val, norm_pos, is_mias)
            self.organize_dataset('test'+str(kfold_index), inputpath, labelpath, outputpath, input_test, label_test, norm_pos, is_mias)
            kfold_index += 1

    def data_split(self, input_list, labels_list, trainsize):
        test_size = 1 - trainsize
        input_train, input_test, label_train, label_test = train_test_split(input_list, labels_list, test_size=test_size)
        input_test, input_val, label_test, label_val = train_test_split(input_test, label_test, test_size=0.5)

        return input_train, input_test, input_val, label_train, label_test, label_val

    def organize_dataset(self, set_type, inputpath, labelpath, outputpath, input_train, label_train, norm_pos=None, is_mias=False, do_augmentation=False, do_denoise=False): #organize dataset in train/valid/test
        #input_set = os.path.sep.join([outputpath, set_type+'_input', set_type])
        #label_set = os.path.sep.join([outputpath, set_type+'_output', set_type])
        input_set = os.path.sep.join([outputpath, set_type+'_input'])
        label_set = os.path.sep.join([outputpath, set_type+'_output'])

        if os.path.isdir(input_set) and os.listdir(input_set):
            shutil.rmtree(input_set)

        if not os.path.isdir(input_set):
            os.makedirs(input_set)

        if os.path.isdir(label_set) and os.listdir(label_set):
            shutil.rmtree(label_set)

        if not os.path.isdir(label_set):
            os.makedirs(label_set)

        totalfiles = len(input_train)
        counter = 0
        for file in tqdm(input_train, desc=input_set, position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
           #print("Copy input files process: {} %".format(round((100/totalfiles)*counter,2)))
           newfilepath = os.path.sep.join([input_set, file])
           origfilepath = os.path.sep.join([inputpath, file])
           shutil.copy2(origfilepath, newfilepath)
           self.imgredimension(newfilepath, 224, 224, norm_pos)
           counter += 1

        totalfiles = len(label_train)
        counter = 0
        for file in tqdm(label_train, desc=label_set, position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
           #print("Copy input files process: {} %".format(round((100/totalfiles)*counter,2)))
           if is_mias: 
               newfilepath = os.path.sep.join([label_set, self.fix_mias_label_filename(file)])
           else:
               newfilepath = os.path.sep.join([label_set, file])
           origfilepath = os.path.sep.join([labelpath, file])
           shutil.copy2(origfilepath, newfilepath)
           #if imgresize:
           #    self.imgresize(newfilepath, imgresize)
           self.imgredimension(newfilepath, 224, 224, norm_pos)
           counter += 1

        if do_denoise:
            self.denoise(input_set, input_set)
        
        if do_augmentation:
            #self.color_augment_images(input_set, label_set, config.AUGMENTATION_NUM_TRIES)
            #self.geometric_augment_images(input_set, label_set, config.AUGMENTATION_NUM_TRIES)
            self.color_and_geometric_augmentation(input_set, label_set, config.AUGMENTATION_NUM_TRIES)

    def fix_mias_label_filename(self, filename):
        return filename.split('_')[0] + '.png'

    def imgredimension(self, imagepath, width, height, norm_pos=None):
        img = cv2.imread(imagepath)
        dim = (width, height)
        img_resize = cv2.resize(img, dim)
        if norm_pos:
            img_resize, _ = self.flip_image(img_resize, norm_pos)
        cv2.imwrite(imagepath, img_resize)

    def imgresize(self, imagepath, imgresize):
        img = cv2.imread(imagepath)
        width = int(img.shape[1] * imgresize)
        height = int(img.shape[0] * imgresize)
        dim = (width, height)
        img_resize = cv2.resize(img, dim)
        cv2.imwrite(imagepath, img_resize)

    def data_gen(self, img_folder, mask_folder, batch_size):
        c = 0
        n = os.listdir(img_folder) #List of training images
        random.shuffle(n)
  
        while (True):
            img = np.zeros((batch_size, 224, 224, 3)).astype('float')
            mask = np.zeros((batch_size, 224, 224, 1)).astype('float')

            for i in range(c, c+batch_size): #initially from 0 to 16, c = 0. 

                train_img = cv2.imread(os.path.sep.join([img_folder, n[i]]))/255.
                train_img =  cv2.resize(train_img, (224, 224))# Read an image from folder and resize
                
                img[i-c] = train_img #add to array - img[0], img[1], and so on.                        
                print(os.path.sep.join([mask_folder, n[i]+"_L"]))
                train_mask = cv2.imread(os.path.sep.join([mask_folder, n[i].split('.')[0]+'_L.png']), cv2.IMREAD_GRAYSCALE)/255.
                train_mask = cv2.resize(train_mask, (224, 224))
                train_mask = train_mask.reshape(224, 224, 1) # Add extra dimension for parity with train_img size [512 * 512 * 3]

                mask[i-c] = train_mask

            c+=batch_size
            if(c+batch_size>=len(os.listdir(img_folder))):
                c=0
                random.shuffle(n)
            yield img, mask

    def seg_model_train(self, inputtrainpath, inputvalpath, outputtrainpath, outputvalpath, outputpath, 
                        segmodel, batchsize, numlabels, numepochs, do_augment):

        Network = self.SEGMODELS[segmodel]

        num_samples = len([f for f in os.listdir(inputtrainpath) if os.path.isfile(os.path.join(inputtrainpath, f))])

        steps_per_epoch = math.ceil(num_samples/batchsize)

        val_num_samples = len([f for f in os.listdir(inputvalpath) if os.path.isfile(os.path.join(inputvalpath, f))])

        val_steps_per_epoch = math.ceil(val_num_samples/batchsize)

        input_height, input_width = self.TRAGETSIZE[segmodel]

        model = Network(n_classes=numlabels, input_height=input_height, input_width=input_width)

        checkpoints_path = os.path.sep.join([outputpath, 'checkpoints'])

        if not os.path.exists(checkpoints_path):
            os.makedirs(checkpoints_path)

        model.train(train_images=inputtrainpath, train_annotations=outputtrainpath, validate=True, val_images=inputvalpath, 
            val_annotations=outputvalpath, epochs=numepochs, batch_size=batchsize, val_batch_size=batchsize,
            steps_per_epoch=steps_per_epoch, val_steps_per_epoch=val_steps_per_epoch, 
            verify_dataset=False, auto_resume_checkpoint=True, do_augment=do_augment,
            checkpoints_path=os.path.sep.join([checkpoints_path, segmodel]))

    def seg_model_train_cross_valid(self, kfolds, datapath, outputpath, segmodel, batchsize, labels, label_names, 
                                numepochs, custom_loss, optimizer_name, metric_name, monitor_name, augmentation_type, with_clw, smooth, denoise, database):
        
        Network = self.SEGMODELS[segmodel]

        numlabels = len(label_names)

        total_iou_list = []
        std_iou_list = []

        acc_mean_list = []
        acc_std_list = []

        acc_classes_mean_list = []
        acc_classes_std_list = []
        
        jaccard_mean_list = []
        jaccard_std_list = []

        jaccard_classes_mean_list = []
        jaccard_classes_std_list = []

        dice_mean_list = []
        dice_std_list = []

        dice_classes_mean_list = []
        dice_classes_std_list = []

        precision_mean_list = []
        precision_std_list = []

        precision_classes_mean_list = []
        precision_classes_std_list = []

        recall_mean_list = []
        recall_std_list = []

        recall_classes_mean_list = []
        recall_classes_std_list = []

        specificity_mean_list = []
        specificity_std_list = []

        specificity_classes_mean_list = []
        specificity_classes_std_list = []

        false_positive_mean_list = []
        false_positive_std_list = []

        false_positive_classes_mean_list = []
        false_positive_classes_std_list = []

        false_negative_mean_list = []
        false_negative_std_list = []

        false_negative_classes_mean_list = []
        false_negative_classes_std_list = []

        auc_mean_list = []
        auc_std_list = []

        auc_classes_mean_list = []
        auc_classes_std_list = []

        csvdata = []
        csv_file = os.path.sep.join([outputpath, segmodel+str(kfolds)+'.csv'])

        #metric_name = 'iou'
        #optimizer_name = 'adam'
        #monitor = 'val_mean_io_u' if metric_name == 'iou' else 'val_loss'

        train_times = []
        test_times = []
        val_losses = []

        for idx in range(0, kfolds):
            print('Fold {} of {}'.format(idx+1, kfolds))
            inputtrainpath = os.path.sep.join( [datapath, 'train'+str(idx)+'_input'] )
            inputvalpath = os.path.sep.join( [datapath, 'val'+str(idx)+'_input'] )
            inputtestpath = os.path.sep.join( [datapath, 'test'+str(idx)+'_input'] )

            outputtrainpath = os.path.sep.join( [datapath, 'train'+str(idx)+'_output'] )
            outputvalpath = os.path.sep.join( [datapath, 'val'+str(idx)+'_output'] )
            outputtestpath = os.path.sep.join( [datapath, 'test'+str(idx)+'_output'] )

            num_samples = len([f for f in os.listdir(inputtrainpath) if os.path.isfile(os.path.join(inputtrainpath, f))])
            steps_per_epoch = math.ceil(num_samples/batchsize)
            val_num_samples = len([f for f in os.listdir(inputvalpath) if os.path.isfile(os.path.join(inputvalpath, f))])
            val_steps_per_epoch = math.ceil(val_num_samples/batchsize)

            input_height, input_width = self.TRAGETSIZE[segmodel]
            model = Network(n_classes=numlabels, input_height=input_height, input_width=input_width)
            checkpoints_path = os.path.sep.join([outputpath, 'checkpoints'])
            print('checkpoints_path: {}'.format(checkpoints_path))
            if not os.path.exists(checkpoints_path):
                os.makedirs(checkpoints_path)
            
            checkpoints_path = os.path.sep.join([checkpoints_path, segmodel])
            checkpoints_path = checkpoints_path + '_' + augmentation_type + '_win' + str(self.TRAGETSIZE[segmodel][0]) + \
                '_metric_' + metric_name + '_loss_' + custom_loss + '_denoise_' + denoise + '_dataset_' + database
            csv_log_filename = segmodel + '_' + augmentation_type + '_win' + str(self.TRAGETSIZE[segmodel][0]) + '_metric_' + \
                metric_name + '_' + database + '.csv'

            csv_log_path = os.path.sep.join([outputpath, 'logs'])
            if not os.path.exists(csv_log_path):
                os.makedirs(csv_log_path)

            csv_log_path = os.path.sep.join([csv_log_path, csv_log_filename])

            if 'val_mean_io_u' in monitor_name and idx > 0:
                monitor_name = monitor_name + '_' + str(idx)

            class_weights = self.calculate_class_weights(outputtrainpath, labels) if with_clw == '1' else None
            #print('class_weights: ', class_weights)

            start_train_time = time.time()

            history = model.train(train_images=inputtrainpath, 
                        train_annotations=outputtrainpath, 
                        validate=True, 
                        val_images=inputvalpath, 
                        val_annotations=outputvalpath, 
                        epochs=numepochs, 
                        batch_size=batchsize, 
                        val_batch_size=batchsize,
                        steps_per_epoch=steps_per_epoch, 
                        val_steps_per_epoch=val_steps_per_epoch, 
                        verify_dataset=False, 
                        auto_resume_checkpoint=False, 
                        #do_augment=True if augmentation_type != 'none' else False,
                        do_augment=False,
                        augmentation_name=augmentation_type, 
                        checkpoints_path=checkpoints_path,
                        csv_log_path=csv_log_path, 
                        metric_name=metric_name, 
                        optimizer_name=optimizer_name,
                        patience=config.PATIENCE, 
                        monitor=monitor_name,
                        class_weights=class_weights,
                        custom_loss=None if custom_loss == 'none' else custom_loss)

            end_train_time = time.time()

            train_times.append(end_train_time-start_train_time)

            if 'val_mean_io_u' in monitor_name:
                monitor_name = 'val_mean_io_u'

            val_losses.append(history.history['val_loss'])
            print('val_losses', val_losses)

            start_test_time = time.time()

            total_iou, std_iou, \
            acc_mean_total, acc_std_total, acc_mean, acc_std, \
            jaccard_mean_total, jaccard_std_total, jaccard_mean, jaccard_std, \
            dice_mean_total, dice_std_total, dice_mean, dice_std, \
            precision_mean_total, precision_std_total, precision_mean, precision_std, \
            recall_mean_total, recall_std_total, recall_mean, recall_std, \
            spc_mean_total, spc_std_total, spc_mean, spc_std, \
            fp_mean_total, fp_std_total, fp_mean, fp_std, \
            fn_mean_total, fn_std_total, fn_mean, fn_std, \
            auc_mean_total, auc_std_total, auc_mean, auc_std = self.seg_evaluate(inputtestpath, outputtestpath, checkpoints_path, 
                                                                            labels, outputpath, label_names, smooth)

            end_test_time = time.time()

            test_times.append(end_test_time-start_test_time)

            total_iou_list.append(total_iou)
            std_iou_list.append(std_iou)

            acc_mean_list.append(acc_mean_total)
            acc_std_list.append(acc_std_total)

            acc_classes_mean_list.append(acc_mean)
            acc_classes_std_list.append(acc_std)

            jaccard_mean_list.append(jaccard_mean_total)
            jaccard_std_list.append(jaccard_std_total)

            jaccard_classes_mean_list.append(jaccard_mean)
            jaccard_classes_std_list.append(jaccard_std)

            dice_mean_list.append(dice_mean_total)
            dice_std_list.append(dice_std_total)

            dice_classes_mean_list.append(dice_mean)
            dice_classes_std_list.append(dice_std)

            precision_mean_list.append(precision_mean_total)
            precision_std_list.append(precision_std_total)

            precision_classes_mean_list.append(precision_mean)
            precision_classes_std_list.append(precision_std)

            recall_mean_list.append(recall_mean_total)
            recall_std_list.append(recall_std_total)

            recall_classes_mean_list.append(recall_mean)
            recall_classes_std_list.append(recall_std)

            specificity_mean_list.append(spc_mean_total)
            specificity_std_list.append(spc_std_total)

            specificity_classes_mean_list.append(spc_mean)
            specificity_classes_std_list.append(spc_std)

            false_positive_mean_list.append(fp_mean_total)
            false_positive_std_list.append(fp_std_total)

            false_positive_classes_mean_list.append(fp_mean)
            false_positive_classes_std_list.append(fp_std)

            false_negative_mean_list.append(fn_mean_total)
            false_negative_std_list.append(fn_std_total)

            false_negative_classes_mean_list.append(fn_mean)
            false_negative_classes_std_list.append(fn_std)

            auc_mean_list.append(auc_mean_total)
            auc_std_list.append(auc_std_total)

            auc_classes_mean_list.append(auc_mean)
            auc_classes_std_list.append(auc_std)

            csvdata.append([idx, total_iou, std_iou])

            #Try to clear GPU memory
            #K.clear_session()
            #del model
            #gc.collect()
            #https://stackoverflow.com/questions/55479221/how-to-clearing-tensorflow-keras-gpu-memory
            K.clear_session()
            del model
            gc.collect()

            print(total_iou_list)
            print(std_iou_list)

        mean_train_time = sum(train_times)/len(train_times)
        mean_test_time = sum(test_times)/len(test_times)

        #val_losses = self.mean_losses(val_losses)
        
        val_losses = self.format_matrix(val_losses)
        print('val_losses', val_losses)

        val_losses_std = np.std(np.array(val_losses), axis=0)
        val_losses = np.mean(np.array(val_losses), axis=0)

        print('val_losses_mean', val_losses)
        print('val_losses_std', val_losses_std)

        self.write_list_to_csv(csvdata, csv_file)
        return total_iou_list, std_iou_list, \
            acc_mean_list, acc_std_list, acc_classes_mean_list, acc_classes_std_list, \
            jaccard_mean_list, jaccard_std_list, jaccard_classes_mean_list, jaccard_classes_std_list, \
            dice_mean_list, dice_std_list, dice_classes_mean_list, dice_classes_std_list, \
            precision_mean_list, precision_std_list, precision_classes_mean_list, precision_classes_std_list, \
            recall_mean_list, recall_std_list, recall_classes_mean_list, recall_classes_std_list, \
            specificity_mean_list, specificity_std_list, specificity_classes_mean_list, specificity_classes_std_list, \
            false_positive_mean_list, false_positive_std_list, false_positive_classes_mean_list, false_positive_classes_std_list, \
            false_negative_mean_list, false_negative_std_list, false_negative_classes_mean_list, false_negative_classes_std_list, \
            auc_mean_list, auc_std_list, auc_classes_mean_list, auc_classes_std_list, \
            mean_train_time, mean_test_time, val_losses, val_losses_std

    def train_all_models_with_cross_valid(self, model_names, kfolds, datapath, modelspath, 
        batchsize, labels, label_names, numepochs, custom_losses, optimizers, metrics, 
        monitors, augmentation_types, with_clw, with_smooth, with_denoise, outputresults='models_results.csv', outputresults_mama='mama_models_results.csv', 
        outputresults_peitoral='peitoral_models_results.csv', outputresults_fundo='fundo_models_results.csv', outputresults_logs='logs.csv', base_name='', database='mias'):

        csv_df = pd.DataFrame()
        csv_df['metrics'] = ['JACCARD', 'DICE', 'PRECISION', 'RECALL', 'ACC', 'SPC', 'FP', 'FN', 'AUC']

        csv_df_mama = pd.DataFrame()
        csv_df_mama['metrics'] = ['JACCARD', 'DICE', 'PRECISION', 'RECALL', 'ACC', 'SPC', 'FP', 'FN', 'AUC']

        csv_df_peitoral = pd.DataFrame()
        csv_df_peitoral['metrics'] = ['JACCARD', 'DICE', 'PRECISION', 'RECALL', 'ACC', 'SPC', 'FP', 'FN', 'AUC']

        csv_df_fundo = pd.DataFrame()
        csv_df_fundo['metrics'] = ['JACCARD', 'DICE', 'PRECISION', 'RECALL', 'ACC', 'SPC', 'FP', 'FN', 'AUC']

        csv_df_logs = pd.DataFrame()
        csv_df_logs['logs'] = ['train_time', 'test_time', 'train_mem_consumption', 'test_mem_consumption']

        csv_df_jaccard = pd.DataFrame()
        csv_df_dice = pd.DataFrame()
        csv_df_acc = pd.DataFrame()

        #csv_df_train_history = pd.DataFrame()

        idx_mama = label_names.index("MAMA")
        idx_peitoral = label_names.index("PEITORAL")
        idx_fundo = label_names.index("FUNDO_REAL")

        model_iou_list = []
        model_iou_std = []
        numlabels = len(label_names)
        count = 0

        trained_models = []
        models_val_losses = []
        models_val_losses_std = []
        models_training_time = []

        distinct_models = []

        for segmodel in model_names:

            """ if not training_with_gpu[count]:
                os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
                os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
            else:
                os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
                os.environ["CUDA_VISIBLE_DEVICES"] = "0" """

            outputpath = os.path.sep.join([modelspath, segmodel])
            ious, stds, \
            acc_means, acc_stds, acc_classes_means, acc_classes_stds, \
            jaccard_means, jaccard_stds, jaccard_classes_means, jaccard_classes_stds, \
            dice_means, dice_stds, dice_classes_means, dice_classes_stds, \
            precision_means, precision_stds, precision_classes_means, precision_classes_stds, \
            recall_means, recall_stds, recall_classes_means, recall_classes_stds, \
            specificity_means, specificity_stds, specificity_classes_means, specificity_classes_stds, \
            false_positive_means, false_positive_stds, false_positive_classes_means, false_positive_classes_stds, \
            false_negative_means, false_negative_stds, false_negative_classes_means, false_negative_classes_stds, \
            auc_means, auc_stds, auc_classes_means, auc_classes_stds, \
            mean_train_time, mean_test_time, val_losses, val_losses_std \
                 = self.seg_model_train_cross_valid(kfolds, datapath, outputpath, 
                segmodel, batchsize, labels, label_names, numepochs, custom_losses[count],
                optimizers[count], metrics[count], monitors[count], augmentation_types[count], with_clw[count], 
                with_smooth[count], with_denoise[count], database)

            model_iou_list.append(ious)
            model_iou_std.append(stds)
            models_val_losses.append(val_losses)
            models_val_losses_std.append(val_losses_std)
            models_training_time.append(mean_train_time)

            if segmodel in trained_models:
                n = trained_models.count(segmodel)
                trained_models.append(segmodel)
                segmodel = segmodel + str(n)
            else:
                trained_models.append(segmodel)

            csv_df_jaccard[segmodel] = jaccard_means
            csv_df_dice[segmodel] = dice_means
            csv_df_acc[segmodel] = acc_means

            distinct_models.append(segmodel)

            #csv_df_train_history[segmodel+'_val_loss'] = pd.Series(val_losses)
            #csv_df_train_history[segmodel+'_std_loss'] = pd.Series(val_losses_std)

            csv_df[segmodel] = [str(np.mean(jaccard_means)) + '+/-' + str(np.mean(jaccard_stds)),
                                str(np.mean(dice_means)) + '+/-' + str(np.mean(dice_stds)), 
                                str(np.mean(precision_means)) + '+/-' + str(np.mean(precision_stds)),
                                str(np.mean(recall_means)) + '+/-' + str(np.mean(recall_stds)),
                                #str(np.mean(ious)) + '+/-' + str(np.mean(stds)),
                                str(np.mean(acc_means)) + '+/-' + str(np.mean(acc_stds)),
                                str(np.mean(specificity_means)) + '+/-' + str(np.mean(specificity_stds)),
                                str(np.mean(false_positive_means)) + '+/-' + str(np.mean(false_positive_stds)),
                                str(np.mean(false_negative_means)) + '+/-' + str(np.mean(false_negative_stds)),
                                str(np.mean(auc_means)) + '+/-' + str(np.mean(auc_stds))]

            csv_df_peitoral[segmodel] = [str(np.mean(jaccard_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(jaccard_classes_stds, axis=0)[idx_peitoral]),
                                str(np.mean(dice_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(dice_classes_stds, axis=0)[idx_peitoral]), 
                                str(np.mean(precision_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(precision_classes_stds, axis=0)[idx_peitoral]),
                                str(np.mean(recall_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(recall_classes_stds, axis=0)[idx_peitoral]),
                                str(np.mean(acc_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(acc_classes_stds, axis=0)[idx_peitoral]),
                                str(np.mean(specificity_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(specificity_classes_stds, axis=0)[idx_peitoral]),
                                str(np.mean(false_positive_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(false_positive_classes_stds, axis=0)[idx_peitoral]),
                                str(np.mean(false_negative_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(false_negative_classes_stds, axis=0)[idx_peitoral]),
                                str(np.mean(auc_classes_means, axis=0)[idx_peitoral]) + '+/-' + str(np.mean(auc_classes_stds, axis=0)[idx_peitoral])]

            csv_df_mama[segmodel] = [str(np.mean(jaccard_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(jaccard_classes_stds, axis=0)[idx_mama]),
                                str(np.mean(dice_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(dice_classes_stds, axis=0)[idx_mama]),  
                                str(np.mean(precision_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(precision_classes_stds, axis=0)[idx_mama]),
                                str(np.mean(recall_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(recall_classes_stds, axis=0)[idx_mama]),
                                str(np.mean(acc_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(acc_classes_stds, axis=0)[idx_mama]),
                                str(np.mean(specificity_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(specificity_classes_stds, axis=0)[idx_mama]),
                                str(np.mean(false_positive_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(false_positive_classes_stds, axis=0)[idx_mama]),
                                str(np.mean(false_negative_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(false_negative_classes_stds, axis=0)[idx_mama]),
                                str(np.mean(auc_classes_means, axis=0)[idx_mama]) + '+/-' + str(np.mean(auc_classes_stds, axis=0)[idx_mama])]

            csv_df_fundo[segmodel] = [str(np.mean(jaccard_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(jaccard_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(dice_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(dice_classes_stds, axis=0)[idx_fundo]),  
                                str(np.mean(precision_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(precision_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(recall_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(recall_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(acc_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(acc_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(specificity_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(specificity_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(false_positive_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(false_positive_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(false_negative_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(false_negative_classes_stds, axis=0)[idx_fundo]),
                                str(np.mean(auc_classes_means, axis=0)[idx_fundo]) + '+/-' + str(np.mean(auc_classes_stds, axis=0)[idx_fundo])]

            csv_df_logs[segmodel] = [mean_train_time, mean_test_time, 0, 0]
            
            count += 1

        results_path = os.path.sep.join([config.RESULTS_PATH, database.upper()])
        if not os.path.exists(results_path):
            os.makedirs(results_path)
            
        csv_df.to_csv( os.path.sep.join([results_path, outputresults]), index=False )
        csv_df_peitoral.to_csv( os.path.sep.join([results_path, outputresults_peitoral]), index=False )
        csv_df_mama.to_csv( os.path.sep.join([results_path, outputresults_mama]), index=False )
        csv_df_fundo.to_csv( os.path.sep.join([results_path, outputresults_fundo]), index=False )
        csv_df_logs.to_csv( os.path.sep.join([results_path, outputresults_logs]), index=False )
        csv_df_jaccard.to_csv( os.path.sep.join([results_path, 'jaccard_' + base_name + '.csv']), index=False )
        csv_df_dice.to_csv( os.path.sep.join([results_path, 'dice_' + base_name + '.csv']), index=False )
        csv_df_acc.to_csv( os.path.sep.join([results_path, 'acc_' + base_name + '.csv']), index=False )

        plotfilepath = os.path.sep.join([results_path, 'plot_training_history_' + base_name + '.png'])

        models_val_losses = self.format_matrix(models_val_losses)
        models_val_losses_std = self.format_matrix(models_val_losses_std)        

        #csv_df_train_history.to_csv( os.path.sep.join([results_path, 'histories_' + base_name + '.csv']), index=False)

        models_val_losses = np.array(models_val_losses)
        models_val_losses_std = np.array(models_val_losses_std)

        self.save_history_csv(models_val_losses.T, models_val_losses_std.T, distinct_models, os.path.sep.join([results_path, 'histories_' + base_name + '.csv']))

        mean_losses = np.mean(models_val_losses, axis=1)
        self.plottraininghistory(models_val_losses, models_val_losses_std, distinct_models, plotfilepath)

        plotfilepath = os.path.sep.join([results_path, 'plot_training_time_' + base_name + '.png'])
        self.plottrainingtime(models_training_time, mean_losses, distinct_models, "Loss", plotfilepath)

        return model_iou_list, model_iou_std

    def save_history_csv(self, vals, std_vals, columns, csvfilepath):
        df = pd.DataFrame()
        for idx in range(0, len(columns)):
            df[columns[idx]+'_val_loss'] = vals[:,idx]
            df[columns[idx]+'_std_loss'] = std_vals[:,idx]
        df.to_csv( csvfilepath, index=False)

    def seg_model_eval(self, inputtestpath, modelpath, labels, outputpath, origsize, smooth, norm_pos=None):
        model = model_from_checkpoint_path(modelpath, True)

        #outputpath = os.path.sep.join([outputpath, model.model_name, 'images'])

        print('outputpath', outputpath)

        if not os.path.exists(outputpath):
            os.makedirs(outputpath)
        
        for imgfile in sorted(os.listdir(inputtestpath)):
            imgfilepath = os.path.sep.join([inputtestpath, imgfile])
            #labelfilepath = os.path.sep.join([outputtestpath, imgfile])

            self.paint_prediction(imgfilepath, model, labels, outputpath, origsize, smooth, norm_pos)

    def paint_prediction(self, inputfilepath, model, labels, outputpath, origsize=False, smooth=False, norm_pos=None):

        output_width = model.output_width
        output_height  = model.output_height
        input_width = model.input_width
        input_height = model.input_height
        n_classes = model.n_classes

        img = cv2.imread(inputfilepath)
        height, width = img.shape[:2]

        flipped = False
        if norm_pos:
            img, flipped = self.flip_image(img, norm_pos)

        #label = cv2.imread(labelfilepath)

        imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)

        pr = model.predict(np.array([imgwindow_arr]))[0]

        pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )

        smooth = True if smooth=='1' else False

        if smooth:
            #pr = self.smoothPrediction(pr, 32, 32, thres)
            pr = self.smoothByMorph(pr)

        #pixels, num_pixels = np.unique(pr, return_counts=True)
        #print('pixels', pixels)
        #print('num_pixels', num_pixels)

        seg_img = np.zeros((output_height, output_width, 3))

        #for c in range(n_classes):
        for c in list(labels.values()):
            color = list(labels.keys())[list(labels.values()).index(c)]
            seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
            seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
            seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')

        if flipped:
            seg_img = cv2.flip(seg_img, 1)
        
        if origsize:
            seg_img = cv2.resize(seg_img, (width, height), interpolation=cv2.INTER_NEAREST)
        else:
            seg_img = cv2.resize(seg_img, (input_width, input_height), interpolation=cv2.INTER_NEAREST)

        cv2.imwrite(os.path.sep.join([ outputpath, 'predseg_' + os.path.basename(inputfilepath) ]), seg_img)

    def seg_evaluate(self, inputtestpath, outputtestpath, modelpath, labels, outputpath, label_names, smooth, norm_pos=None):
        print('loading model from: {}'.format(modelpath))
        model = model_from_checkpoint_path(modelpath, True)

        outputpath = os.path.sep.join([outputpath, model.model_name])

        if not os.path.exists(outputpath):
            os.makedirs(outputpath)

        conf_matrix = np.zeros((len(labels), len(labels)))
        final_ious = []
        classes_iou_list = []
        expected_list = []
        acc_list = []
        recall_list = []
        precision_list = []
        jaccard_list = []
        dice_list = []
        fp_list = []
        fn_list = []
        tp_list = []
        tn_list = []
        spc_list = []
        auc_list = []

        for imgfile in sorted(os.listdir(inputtestpath)):
            imgfilepath = os.path.sep.join([inputtestpath, imgfile])
            labelfilepath = os.path.sep.join([outputtestpath, imgfile])
            conf_matrix, acc_list, recall_list, precision_list, jaccard_list, dice_list, \
            fp_list, fn_list, tp_list, tn_list, spc_list, auc_list = self.evaluate(imgfilepath, labelfilepath, model, labels, 
                          conf_matrix, final_ious, classes_iou_list, expected_list, acc_list, recall_list, precision_list, jaccard_list, dice_list, 
                          fp_list, fn_list, tp_list, tn_list, spc_list, auc_list, smooth, norm_pos)

        classes_iou_list = np.array( classes_iou_list )
        expected_list = np.array( expected_list )
        final_classes_iou, std_classes_iou = self.columnsMeanStd(classes_iou_list, expected_list)
        final_ious = np.array( final_ious )
        total_iou = np.mean(final_ious, axis=0)
        std_iou = np.std(final_ious)

        acc_list = np.array(acc_list)
        acc_mean = np.mean(acc_list, axis=0)
        acc_std = np.std(acc_list, axis=0)
        acc_mean_total = np.mean(acc_mean)
        acc_std_total = np.std(acc_std)

        precisions, recalls = self.precision_and_recall(conf_matrix)

        recall_list = np.array(recall_list)
        recall_mean = np.mean(recall_list, axis=0)
        recall_std = np.std(recall_list, axis=0)
        recall_mean_total = np.mean(recall_mean)
        recall_std_total = np.mean(recall_std)

        precision_list = np.array(precision_list)
        precision_mean = np.mean(precision_list, axis=0)
        precision_std = np.std(precision_list, axis=0)
        precision_mean_total = np.mean(precision_mean)
        precision_std_total = np.mean(precision_std)

        jaccard_list = np.array(jaccard_list)
        jaccard_mean = np.mean(jaccard_list, axis=0)
        jaccard_std = np.std(jaccard_list, axis=0)
        jaccard_mean_total = np.mean(jaccard_mean)
        jaccard_std_total = np.mean(jaccard_std)

        dice_list = np.array(dice_list)
        dice_mean = np.mean(dice_list, axis=0)
        dice_std = np.std(dice_list, axis=0)
        dice_mean_total = np.mean(dice_mean)
        dice_std_total = np.mean(dice_std)

        spc_list = np.array(spc_list)
        spc_mean = np.mean(spc_list, axis=0)
        spc_std = np.std(spc_list, axis=0)
        spc_mean_total = np.mean(spc_mean)
        spc_std_total = np.mean(spc_std)

        fp_list = np.array(fp_list)
        fp_mean = np.mean(fp_list, axis=0)
        fp_std = np.std(fp_list, axis=0)
        fp_mean_total = np.mean(fp_mean)
        fp_std_total = np.mean(fp_std)

        fn_list = np.array(fn_list)
        fn_mean = np.mean(fn_list, axis=0)
        fn_std = np.std(fn_list, axis=0)
        fn_mean_total = np.mean(fn_mean)
        fn_std_total = np.mean(fn_std)

        auc_list = np.array(auc_list)
        auc_mean = np.mean(auc_list, axis=0)
        auc_std = np.std(auc_list, axis=0)
        auc_mean_total = np.mean(auc_mean)
        auc_std_total = np.mean(auc_std)

        conf_matrix = conf_matrix / np.max(np.abs(conf_matrix))
        print('IoU mean: {} with std {}'.format(total_iou, std_iou))
        print("IoU mean for each class: {} with std {}".format(final_classes_iou, std_classes_iou))

        print("Manual precisions: {} and recalls {}".format(precisions, recalls))

        print('classes accuracy: {} with std {}'.format(acc_mean, acc_std))
        print('mean accuracy: {} with std {}'.format(acc_mean_total, acc_std_total))

        print("Sklearn classes recalls: {} with std {}".format(recall_mean, recall_std))
        print("Sklearn Mean recall: {} with std {}".format(recall_mean_total, recall_std_total))

        print("Sklearn classes precisions: {} with std {}".format(precision_mean, precision_std))
        print("Sklearn Mean precision: {} with std {}".format(precision_mean_total, precision_std_total))

        print("Sklearn classes jaccard: {} with std {}".format(jaccard_mean, jaccard_std))
        print("Sklearn Mean jaccard: {} with std {}".format(jaccard_mean_total, jaccard_std_total))

        print("Sklearn classes dice: {} with std {}".format(dice_mean, dice_std))
        print("Sklearn Mean dice: {} with std {}".format(dice_mean_total, dice_std_total))

        print("Manual classes specificity: {} with std {}".format(spc_mean, spc_std))
        print("Manual Mean specificity: {} with std {}".format(spc_mean_total, spc_std_total))

        print("Manual classes FalsePositive: {} with std {}".format(fp_mean, fp_std))
        print("Manual Mean FalsePositive: {} with std {}".format(fp_mean_total, fp_std_total))

        print("Manual classes FalseNegative: {} with std {}".format(fn_mean, fn_std))
        print("Manual Mean FalseNegative: {} with std {}".format(fn_mean_total, fn_std_total))

        print("Sklearn classes AUC: {} with std {}".format(auc_mean, auc_std))
        print("Sklearn Mean AUC: {} with std {}".format(auc_mean_total, auc_std_total))

        sum_of_rows = conf_matrix.sum(axis=1)
        conf_matrix_normalized = conf_matrix / sum_of_rows[:, np.newaxis]
        conf_matrix_normalized = np.nan_to_num(conf_matrix_normalized, 0)
        print("confusion matrix", conf_matrix_normalized)
        np.savetxt(os.path.sep.join([ outputpath, 'matriz_confusao.csv' ]), conf_matrix_normalized, delimiter=",")
        plotpath = os.path.sep.join([ outputpath, 'matriz_confusao.png' ])
        self.plotConfMatrix(conf_matrix_normalized, label_names, plotpath)

        return total_iou, std_iou, \
            acc_mean_total, acc_std_total, acc_mean, acc_std, \
            jaccard_mean_total, jaccard_std_total, jaccard_mean, jaccard_std, \
            dice_mean_total, dice_std_total, dice_mean, dice_std, \
            precision_mean_total, precision_std_total, precision_mean, precision_std, \
            recall_mean_total, recall_std_total, recall_mean, recall_std, \
            spc_mean_total, spc_std_total, spc_mean, spc_std, \
            fp_mean_total, fp_std_total, fp_mean, fp_std, \
            fn_mean_total, fn_std_total, fn_mean, fn_std, \
            auc_mean_total, auc_std_total, auc_mean, auc_std

    #https://stackoverflow.com/questions/40729875/calculate-precision-and-recall-in-a-confusion-matrix
    def precision_and_recall(self, confusion_matrix):
        true_pos = np.diag(confusion_matrix) 
        precisions = true_pos / np.sum(confusion_matrix, axis=0)
        recalls = true_pos / np.sum(confusion_matrix, axis=1)
        return precisions, recalls

    def plotConfMatrix(self, confMatrix, labels, plotpath):
      leftmargin = 1.5 # inches
      rightmargin = 1.5 # inches
      categorysize = 1.5 # inches
      figwidth = leftmargin + rightmargin + (len(labels) * categorysize)
      fig = plt.figure(figsize=(figwidth, figwidth))
      ax = fig.add_subplot(111)
      ax.set_aspect(1)
      fig.subplots_adjust(left=leftmargin/figwidth, right=1-rightmargin/figwidth, top=0.94, bottom=0.1)
      cax = ax.matshow(confMatrix)

      for i in range(confMatrix.shape[0]):
        for j in range(confMatrix.shape[1]):
          c = confMatrix[j,i]
          if c > 0.6:
              color = 'black'
          else:
              color = 'w'
          ax.text(i, j, str(np.round(c,2)),color=color, va='center', ha='center')

      plt.title('Confusion matrix')
      fig.colorbar(cax)
      ax.set_xticks(range(len(labels)))
      ax.set_yticks(range(len(labels)))
      ax.set_xticklabels(labels)
      ax.set_yticklabels(labels)
      ax.set_ylim(len(labels)-0.5, 0-0.5)

      plt.xlabel('Predicted')
      plt.ylabel('True')

      plt.savefig(plotpath)

    """ def plotConfMatrix(self, confMatrix, labels, plotpath):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        cax = ax.matshow(confMatrix)
        plt.title('Confusion matrix')
        fig.colorbar(cax)
        ax.set_xticklabels([''] + labels)
        ax.set_yticklabels([''] + labels)
        ax.xaxis.set_major_locator(MultipleLocator(1))
        ax.yaxis.set_major_locator(MultipleLocator(1))
        plt.xlabel('Predicted')
        plt.ylabel('True')
        plt.savefig(plotpath) """      

    def columnsMeanStd(self, nd_array, expected_list):
        col_means = []
        col_stds = []
        for col_idx in range(0, nd_array.shape[1]):
            col_values = np.take(nd_array[:,col_idx], np.nonzero(expected_list[:,col_idx]))
            col_means.append(np.mean(col_values))
            col_stds.append(np.std(col_values))

        return np.nan_to_num(col_means, 0), np.nan_to_num(col_stds, 0)

    def evaluate(self, inputfilepath, labelfilepath, model, labels, 
                 conf_matrix, final_ious, classes_iou_list, expected_list, 
                 acc_list, recall_list, precision_list, jaccard_list, dice_list, 
                 fp_list, fn_list, tp_list, tn_list, spc_list, auc_list, smooth, norm_pos=None):
        output_width = model.output_width
        output_height  = model.output_height
        input_width = model.input_width
        input_height = model.input_height
        n_classes = model.n_classes

        img = cv2.imread(inputfilepath)
        label = cv2.imread(labelfilepath)

        if norm_pos:
            img, _ = self.flip_image(img, norm_pos)
            label, _ = self.flip_image(label, norm_pos)

        imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)
        pr = model.predict(np.array([imgwindow_arr]))[0]
        pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )

        smooth = True if smooth=='1' else False

        if smooth:
            #pr = self.smoothPrediction(pr, 32, 32, thres)
            pr = self.smoothByMorph(pr)

        pr = pr.reshape((output_width*output_height))

        #pixels, num_pixels = np.unique(pr, return_counts=True)
        #print('pixels', pixels)
        #print('num_pixels', num_pixels)

        #pixels, num_pixels = np.unique(label, return_counts=True)
        #print('pixels label', pixels)
        #print('num_pixels label', num_pixels)

        gt = get_segmentation_array(label, n_classes, output_width, output_height)
        gt = gt.argmax(-1)

        #pixels, num_pixels = np.unique(gt, return_counts=True)
        #print('pixels out', pixels)
        #print('num_pixels out', num_pixels)

        curr_conf_matrix = confusion_matrix(gt, pr, list(range(0, n_classes)))
        conf_matrix = conf_matrix + curr_conf_matrix
        acc, recall, precision, jaccard, dice, auc = self.performance_scores(gt, pr, list(range(0, n_classes)))

        false_positive, false_negative, true_positive, true_negative, specificity, false_positive_rate, \
        false_negative_rate = self.confusion_matrix_metrics(curr_conf_matrix)

        acc_list.append(acc)
        recall_list.append(recall)
        precision_list.append(precision)
        jaccard_list.append(jaccard)
        dice_list.append(dice)
        fp_list.append(false_positive_rate)
        fn_list.append(false_negative_rate)
        tp_list.append(true_positive)
        tn_list.append(true_negative)
        spc_list.append(specificity)
        auc_list.append(auc)
        expected = np.zeros(n_classes, dtype=np.uint8)
        for v in gt:
            expected[v] = 1
        classes_iou = metrics.get_iou(gt, pr, n_classes)
        classes_iou_list.append(classes_iou)
        expected_list.append(expected)
        final_iou = np.mean( np.take(classes_iou, np.nonzero(expected)) )
        final_ious.append( final_iou )

        return conf_matrix, acc_list, recall_list, precision_list, jaccard_list, dice_list, fp_list, fn_list, tp_list, tn_list, spc_list, auc_list

    def performance_scores(self, y_true, y_pred, labels):
        #acc_total = accuracy_score(y_true, y_pred)
        cmat = confusion_matrix(y_true, y_pred, labels)
        acc = cmat.diagonal()/cmat.sum(axis=1)
        acc = [0 if math.isnan(x) else x for x in acc]
        recall = recall_score(y_true, y_pred, labels, average=None)
        precision = precision_score(y_true, y_pred, labels, average=None)
        jaccard = jaccard_score(y_true, y_pred, labels, average=None)
        #dice = self.dice_coef(y_true, y_pred)
        dice = f1_score(y_true, y_pred, labels, average=None)

        y_true_2d = []
        for y in y_true:
            temp = np.zeros(len(labels), dtype=int)
            temp[y] = 1
            y_true_2d.append(temp)

        y_pred_2d = []
        for y in y_pred:
            temp = np.zeros(len(labels), dtype=int)
            temp[y] = 1
            y_pred_2d.append(temp)
        
        try:
            auc = roc_auc_score(y_true_2d, y_pred_2d, labels=labels, average=None, multi_class='ovr')
        except:
            auc = np.ones(len(labels))
        
        return acc, recall, precision, jaccard, dice, auc

    #https://stackoverflow.com/questions/31324218/scikit-learn-how-to-obtain-true-positive-true-negative-false-positive-and-fal
    #https://towardsdatascience.com/multi-class-classification-extracting-performance-metrics-from-the-confusion-matrix-b379b427a872
    def confusion_matrix_metrics(self, confusion_matrix):
        FP = confusion_matrix.sum(axis=0) - np.diag(confusion_matrix)  
        FN = confusion_matrix.sum(axis=1) - np.diag(confusion_matrix)
        TP = np.diag(confusion_matrix)
        TN = confusion_matrix.sum() - (FP + FN + TP)

        # Sensitivity, hit rate, recall, or true positive rate
        TPR = TP/(TP+FN)
        # Specificity or true negative rate
        TNR = TN/(TN+FP) 
        # Precision or positive predictive value
        PPV = TP/(TP+FP)
        # Negative predictive value
        NPV = TN/(TN+FN)
        # Fall out or false positive rate
        FPR = FP/(FP+TN)
        FPR = [0 if math.isnan(x) else x for x in FPR]
        # False negative rate
        FNR = FN/(TP+FN)
        FNR = [0 if math.isnan(x) else x for x in FNR]
        # False discovery rate
        FDR = FP/(TP+FP)

        # Overall accuracy
        ACC = (TP+TN)/(TP+FP+FN+TN)
        #return FP, FN, TP, TN, TPR, TNR, PPV, NPV, FPR, FNR, FDR, ACC
        return FP, FN, TP, TN, TNR, FPR, FNR

    def dice_coef(self, y_true, y_pred, epsilon=1e-6):
        """Altered Sorensen–Dice coefficient with epsilon for smoothing."""
        y_true_flatten = np.asarray(y_true).astype(np.bool)
        y_pred_flatten = np.asarray(y_pred).astype(np.bool)

        if not np.sum(y_true_flatten) + np.sum(y_pred_flatten):
            return 1.0

        return (2. * np.sum(y_true_flatten * y_pred_flatten)) / (np.sum(y_true_flatten) + np.sum(y_pred_flatten) + epsilon)

    def label2segmented(self, outputtestpath, labels, outputmaskpath):
        if not os.path.exists(outputmaskpath):
            os.mkdir(outputmaskpath)

        for imgfile in sorted(os.listdir(outputtestpath)):
            label_img = cv2.imread( os.path.sep.join([outputtestpath, imgfile]) )
            seg_img = label_img.copy()
            for k,v in labels.items():
                seg_img[np.where((seg_img==[v]).all(axis=2))] = k

            output_file_path = os.path.sep.join([ outputmaskpath, imgfile ])
            cv2.imwrite(output_file_path, cv2.cvtColor(seg_img, cv2.COLOR_RGB2BGR))

    def ttest(self, csvfile):
        models_df = pd.read_csv(csvfile)
        num_columns = len(models_df.columns)
        combinations = list(itertools.combinations(range(num_columns), 2))
        for pair in combinations:
            modelA = models_df.iloc[:,pair[0]]
            modelB = models_df.iloc[:,pair[1]]
            t_test, pval = ttest_ind(modelA, modelB)
            print('Compare models: {} and {}'.format(modelA.name, modelB.name))
            print("p-value", pval)

    def tukey_test(self, csvfile):
        models_df = pd.read_csv(csvfile)
        models_list = models_df.values
        values_list = np.concatenate( (models_list[:,0], models_list[:,1], 
                                        models_list[:,2], models_list[:,3], models_list[:,4]) )
        df = pd.DataFrame({'score': values_list,
                   'group': np.repeat(list(models_df.columns), repeats=models_df.shape[0])})

        tukey = pairwise_tukeyhsd(endog=df['score'],
                          groups=df['group'],
                          alpha=0.05)
        print(tukey)

    """ def ftest(self, csvfile):
        models_df = pd.read_csv(csvfile)
        num_columns = len(models_df.columns)
        if num_columns == 5:
            self.ttest(csvfile)
        elif num_columns == 3:
            modelA = models_df.iloc[:,0]
            modelB = models_df.iloc[:,1]
            modelC = models_df.iloc[:,2]
            modelD = models_df.iloc[:,3]
            modelE = models_df.iloc[:,4]
            f_test, pval = f_oneway(modelA, modelB, modelC, modelD, modelE)
            print('Compare models: {}, {}, {}, {} and {}'.format(modelA.name, modelB.name, modelC.name, modelD.name, modelE.name))
            print("p-value", pval)
            print('f-test', f_test) """

    def ftest2(self, csvfile):
        models_df = pd.read_csv(csvfile)
        num_columns = len(models_df.columns)
        pval = 1
        if num_columns == 2:
            modelA = models_df.iloc[:,0]
            modelB = models_df.iloc[:,1]
            t_test, pval = ttest_ind(modelA, modelB)
            print('Compare models: {} and {}'.format(modelA.name, modelB.name))
            print("p-value", pval)
        elif num_columns == 5:
            modelA = models_df.iloc[:,0]
            modelB = models_df.iloc[:,1]
            modelC = models_df.iloc[:,2]
            modelD = models_df.iloc[:,3]
            modelE = models_df.iloc[:,4]
            f_test, pval = f_oneway(modelA, modelB, modelC, modelD, modelE)
            print('Compare models: {}, {}, {}, {} and {}'.format(modelA.name, modelB.name, modelC.name, modelD.name, modelE.name))
            print("p-value", pval)
        if pval < 0.05:
            print('Testing combination of models in pairs')
            self.ttest(csvfile)

    def plotAcc(self, accuracies, deviations, model_names, plotpath):
        num_models = len(accuracies)
        num_folds = len(accuracies[0])
        folds_list = list(range(num_folds))
        for idx in range(num_models):
            plt.errorbar(folds_list, accuracies[idx], deviations[idx], linestyle='None', marker='^')
        plt.xlabel('Folds')
        plt.ylabel('Accuracies')
        plt.legend(model_names)
        plt.savefig(plotpath)

    def plotBoxPlot(self, csvfile, savepath, metric, title):
        models_df = pd.read_csv(csvfile)
        fig = plt.figure()
        boxplot = models_df.boxplot()
        plt.title(title)
        plt.ylabel(metric)
        plt.xticks(rotation=45)
        fig.tight_layout()
        fig.savefig(savepath)

    def joinBoxPlots(self, csvfile1, csvfile2, savepath, metric1, metric2):
        models_df1 = pd.read_csv(csvfile1)
        models_df2 = pd.read_csv(csvfile2)
        #fig, axes = plt.subplots(2,1) # create figure and axes
        fig=plt.figure()
        ax1 = fig.add_subplot(2, 1, 1)
        boxplot = models_df1.boxplot()
        ax1.set_ylabel(metric1)
        ax1.axes.xaxis.set_ticklabels([])

        ax2 = fig.add_subplot(2, 1, 2)
        boxplot = models_df2.boxplot()
        ax2.set_ylabel(metric2)
        fig.tight_layout()
        fig.savefig(savepath)

    def joinBoxPlots2(self, csvfile1, csvfile2, csvfile3, csvfile4, savepath, metric1, metric2):
        models_df1 = pd.read_csv(csvfile1)
        models_df2 = pd.read_csv(csvfile2)
        models_df3 = pd.read_csv(csvfile3)
        models_df4 = pd.read_csv(csvfile4)
        fig=plt.figure(figsize=(10, 5))
        #yticks = ticker.MaxNLocator(4)
        ax1 = fig.add_subplot(2, 2, 1)
        boxplot = models_df1.boxplot()
        ax1.set_ylabel(metric1)
        ax1.axes.xaxis.set_ticklabels([])
        ax1.set_title('Before Optimization')
        #ax1.yaxis.set_major_locator(plt.MaxNLocator(4))
        ax1.set_yticks([0.86, 0.92, 1.0])

        ax2 = fig.add_subplot(2, 2, 2)
        boxplot = models_df2.boxplot()
        #ax2.axes.yaxis.set_ticklabels([])
        ax2.axes.xaxis.set_ticklabels([])
        ax2.set_title('After Optimization')
        ax2.set_yticks([0.86, 0.92, 1.0])
        ax2.axes.yaxis.set_ticklabels([])
        #ax2.yaxis.set_major_locator(plt.MaxNLocator(4))

        ax3 = fig.add_subplot(2, 2, 3)
        boxplot = models_df3.boxplot()
        ax3.set_ylabel(metric2)
        ax3.set_ylabel(metric2)
        plt.setp(ax3, xticklabels=['Resnet50-Unet', 'Mobilenet-Unet', 'Vgg-Unet', 'Unet', 'Segnet'])
        #ax3.yaxis.set_major_locator(plt.MaxNLocator(4))
        ax3.set_yticks([0.86, 0.92, 1.0])
        ax3.tick_params(axis='x', labelrotation=45)

        ax4 = fig.add_subplot(2, 2, 4)
        boxplot = models_df4.boxplot()
        #ax4.axes.yaxis.set_ticklabels([])
        plt.setp(ax4, xticklabels=['Resnet50-Unet', 'Mobilenet-Unet', 'Vgg-Unet', 'Unet', 'Segnet'])
        #ax4.yaxis.set_major_locator(plt.MaxNLocator(4))
        ax4.set_yticks([0.86, 0.92, 1.0])
        ax4.axes.yaxis.set_ticklabels([])
        ax4.tick_params(axis='x', labelrotation=45)
        
        fig.tight_layout()
        fig.savefig(savepath)

    def imagesBlend(self, imagepathA, imagepathB, outputpath):
        imgA = cv2.imread(imagepathA)
        imgB = cv2.imread(imagepathB)
        imgAB = cv2.addWeighted(imgA, 0.7, imgB, 0.3, 0)
        cv2.imwrite(outputpath, imgAB)

    def joinImagesAndMasks(self, imagespath, maskspath, outputpath):
        if not os.path.exists(outputpath):
            os.mkdir(outputpath)

        for maskfile in os.listdir(maskspath):
            image_name = maskfile.split('_')[1]
            imagepath = os.path.sep.join([imagespath, image_name])
            if os.path.exists(imagepath):
                maskpath = os.path.sep.join([maskspath, maskfile])
                outputImagePath = os.path.sep.join([outputpath, image_name])
                self.imagesBlend(imagepath, maskpath, outputImagePath)

    def dicomFolder2Png(self, inputpath, outputpath, isinbreast):
        if not os.path.exists(outputpath):
            os.mkdir(outputpath)

        for file in os.listdir(inputpath):
            print('converting file: {} to PNG'.format(file))
            #ds = pydicom.read_file(os.path.sep.join([inputpath, file]))
            #img = ds.pixel_array
            #cv2.imwrite(os.path.sep.join([outputpath, file.replace('.dcm', '.png')]), img)
            if os.path.splitext(file)[1] == '.dcm':
                self.dicom2png( os.path.sep.join([inputpath, file]), os.path.sep.join([outputpath, file.replace('.dcm', '.png')]), isinbreast )

    def dicom2png(self, inputfilepath, outputfilepath, isinbreast=False):
        ds = pydicom.read_file(inputfilepath)
        img = ds.pixel_array
        if isinbreast:
           img = img*22 
        cv2.imwrite(outputfilepath, img)

    def getCoordsFromJson(self, jsonpath):
        x_arr_list = []
        y_arr_list = []
        type_reg_list = []
        with open(jsonpath) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                x_arr = json.loads(row['region_shape_attributes'])['all_points_x']
                y_arr = json.loads(row['region_shape_attributes'])['all_points_y']
                type_reg = list(json.loads(row['region_attributes']).keys())[0]
                x_arr_list.append(x_arr)
                y_arr_list.append(y_arr)
                type_reg_list.append(type_reg)
        return x_arr_list, y_arr_list, type_reg_list

    def getCoordsFromJsonLibra(self, jsonpath):
        x_arr_list = []
        y_arr_list = []
        file_name_list = []
        with open(jsonpath) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                x_arr = json.loads(row['region_shape_attributes'])['all_points_x']
                y_arr = json.loads(row['region_shape_attributes'])['all_points_y']
                filename = row['filename']
                x_arr_list.append(x_arr)
                y_arr_list.append(y_arr)
                file_name_list.append(filename)
        return x_arr_list, y_arr_list, file_name_list

    def getCoordsFromCsv(self, csvfile):
        x_arr_list = []
        y_arr_list = []
        file_name_list = []
        mask_type_list = []
        with open(csvfile) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                if int(row['region_count']) > 0:
                    file_name_list.append(row['filename'])
                    x_arr_list.append(json.loads(row['region_shape_attributes'])['all_points_x'])
                    y_arr_list.append(json.loads(row['region_shape_attributes'])['all_points_y'])
                    mask_dict = json.loads(row['region_attributes'])
                    if not mask_dict.items():
                        print('empty label in: ', row['filename'])
                    mask_type_list.append([k for k, v in mask_dict.items() if v][0])

        return file_name_list, x_arr_list, y_arr_list, mask_type_list

    def parseCoordsFromCsv(self, csvfile):
        x_arr_list = []
        y_arr_list = []
        file_name_list = []
        mask_type_list = []
        with open(csvfile) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
            for row in csv_reader:
                file_name_list.append(row['filename'])
                mask_type_list.append(row['label'])
                x_arr_list.append(row['x'])
                y_arr_list.append(row['y'])

        return file_name_list, x_arr_list, y_arr_list, mask_type_list
              
    def createMasksFromCoordsV2(self, imagesfolder, csvpath, labels, label_names, outputpath_masks, 
                                outputpath_labels, default_background):
        if not os.path.exists(outputpath_masks):
            os.makedirs(outputpath_masks)
        if not os.path.exists(outputpath_labels):
            os.makedirs(outputpath_labels)
        #file_name_list, X_arr_list, Y_arr_list, mask_type_list = self.getCoordsFromCsv(csvpath)
        file_name_list, X_arr_list, Y_arr_list, mask_type_list = self.parseCoordsFromCsv(csvpath)
        unique_filenames = np.unique( np.array(file_name_list) )
        #print(file_name_list)
        for filename in tqdm(unique_filenames, desc='Images rate:', position=0, 
                leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
            #print('mask for file: {}'.format(filename))
            imagepath = os.path.sep.join([imagesfolder, filename])
            img = cv2.imread(imagepath)
            height, width = img.shape[:2]
            mask = np.zeros(shape=[height, width, 3], dtype=np.uint8)
            label = np.zeros(shape=[height, width, 3], dtype=np.uint8)
            label_idx = label_names.index(default_background)
            color = list(labels.keys())[label_idx]
            label_code = list(labels.values())[label_idx]
            mask[:] = color[::-1]
            label[:] = label_code
            file_idxs = np.where(np.array(file_name_list) == filename)[0]
            for idx in file_idxs:
                X_arr = ast.literal_eval(X_arr_list[idx])
                Y_arr = ast.literal_eval(Y_arr_list[idx])
                mask_type = mask_type_list[idx]
                #print('mask type: {}'.format(mask_type))
                label_idx = label_names.index(mask_type)
                color = list(labels.keys())[label_idx]
                label_code = list(labels.values())[label_idx]
                coords = list(zip(X_arr, Y_arr))
                polygon = Polygon(coords)
                int_coords = lambda x: np.array(x).round().astype(np.int32)
                exterior = [int_coords(polygon.exterior.coords)]
                cv2.fillPoly(label, exterior, color=(label_code,label_code,label_code))
                cv2.fillPoly(mask, exterior, color=color[::-1])
            outputmaskpath = os.path.sep.join([outputpath_masks, filename])
            outputlabelpath = os.path.sep.join([outputpath_labels, filename])
            cv2.imwrite(outputmaskpath, mask)
            cv2.imwrite(outputlabelpath, label)

    def createMaskFromCoords(self, imagepath, jsonpath, labels, label_names, outputpath):
        img = cv2.imread(imagepath)
        height, width = img.shape[:2]
        mask = np.zeros(shape=[height, width, 3], dtype=np.uint8)
        label = np.zeros(shape=[height, width, 3], dtype=np.uint8)
        X_arr_list, Y_arr_list, type_list = self.getCoordsFromJson(jsonpath)
        for idx in range(len(X_arr_list)):
            X_arr = X_arr_list[idx]
            Y_arr = Y_arr_list[idx]
            obj_type = type_list[idx]
            label_idx = label_names.index(obj_type)
            color = list(labels.keys())[label_idx]
            label_code = list(labels.values())[label_idx]
            coords = list(zip(X_arr, Y_arr))
            polygon = Polygon(coords)
            int_coords = lambda x: np.array(x).round().astype(np.int32)
            exterior = [int_coords(polygon.exterior.coords)]
            cv2.fillPoly(label, exterior, color=(label_code,label_code,label_code))
            cv2.fillPoly(mask, exterior, color=color[::-1])
        cv2.imwrite(outputpath, mask)
        cv2.imwrite(outputpath, label)

    def createMasksFromCoordsLibra(self, imagefolderpath, csvfile, color, label, outputpath_masks, outputpath_labels):
        print('read coords csv file')
        X_arr_list, Y_arr_list, file_name_list = self.getCoordsFromJsonLibra(csvfile)
        print('reading images')
        for img in os.listdir(imagefolderpath):
            print('image: {}'.format(img))
            img_idx = file_name_list.index(img)
            X_arr = X_arr_list[img_idx]
            Y_arr = Y_arr_list[img_idx]
            imagepath = os.path.sep.join([imagefolderpath, img])
            outputmaskpath = os.path.sep.join([outputpath_masks, img])
            outputlabelpath = os.path.sep.join([outputpath_labels, img])
            self.maskImageFromCoord(imagepath, img, X_arr, Y_arr, color, label, outputmaskpath, outputlabelpath)

    def maskImageFromCoord(self, imagepath, filename, X_arr, Y_arr, color, label, outputmaskpath, outputlabelpath):
        img = cv2.imread(imagepath)
        height, width = img.shape[:2]
        color_mask = np.zeros(shape=[height, width, 3], dtype=np.uint8)
        label_mask = np.zeros(shape=[height, width, 3], dtype=np.uint8)
        coords = list(zip(X_arr, Y_arr))
        polygon = Polygon(coords) #polygon = Polygon([(x[0], y[0]), (x[1], y[1]), (x[2], y[2]), (x[0], y[2])])
        int_coords = lambda x: np.array(x).round().astype(np.int32)
        exterior = [int_coords(polygon.exterior.coords)]
        cv2.fillPoly(color_mask, exterior, color=color[::-1])
        cv2.fillPoly(label_mask, exterior, color=label)
        cv2.imwrite(outputmaskpath, color_mask)
        cv2.imwrite(outputlabelpath, label_mask)

    def re_color_database(self, labelpath, maskpath, orig_classes, dest_classes, 
                orig_colors, dest_colors, newlabelpath, newmaskpath):

        if not os.path.exists(newlabelpath):
            os.makedirs(newlabelpath)

        if not os.path.exists(newmaskpath):
            os.makedirs(newmaskpath)

        for label in os.listdir(labelpath):

            print('file name: ', label)
            label_img = cv2.imread( os.path.sep.join([labelpath, label]) )
            mask_img = cv2.imread( os.path.sep.join([maskpath, label]) )
            for idx in range(len(orig_classes)):
                orig_class = orig_classes[idx]
                dest_class = dest_classes[idx]
                background_orig_pixel = (orig_class,orig_class,orig_class)
                background_dest_pixel = (dest_class,dest_class,dest_class)
                label_img[np.all(label_img == background_orig_pixel, axis=-1)] = background_dest_pixel

                background_orig_pixel = orig_colors[idx]
                background_dest_pixel = dest_colors[idx]
                mask_img[np.all(mask_img == background_orig_pixel[::-1], axis=-1)] = background_dest_pixel[::-1]

            cv2.imwrite( os.path.sep.join([newlabelpath, label]), label_img )
            cv2.imwrite( os.path.sep.join([newmaskpath, label]), mask_img )

    def label2mask(self, labelpath, labels, maskpath):
        if not os.path.exists(maskpath):
            os.makedirs(maskpath)

        for label in os.listdir(labelpath):
            print('label file name: ', label)
            img = cv2.imread( os.path.sep.join([labelpath, label]) )
            #height, width = img.shape[:2]
            for color in labels:
                idx = labels[color]
                img[np.all(img == (idx, idx, idx), axis=-1)] = color[::-1]

            cv2.imwrite( os.path.sep.join([maskpath, label]), img )

    def copyDDSMFiles(self, inputpath, outputpath):
        if not os.path.exists(outputpath):
            os.mkdir(outputpath)

        totalFolders = len(os.listdir(inputpath))
        for folder in os.listdir(inputpath):
            print('folder: {}'.format(folder))
            totalFolders = totalFolders-1
            print('remain folders: {}'.format(totalFolders))
            folderType = folder.split('-')[0]
            folderSet = folder.split('-')[1].split('_')[0]
            baseFileName = '_'.join(folder.split('-')[1].split('_')[1:])
            finalFolderPath = os.path.sep.join([outputpath, folderType, folderSet])
            if not os.path.exists(finalFolderPath):
                os.makedirs(finalFolderPath)
            for currentFolderPath, subFolders, images in os.walk(os.path.sep.join([inputpath, folder])):
                if len(images)==1:
                    inputFilePath = os.path.sep.join([currentFolderPath, images[0]])
                    outputFilePath = os.path.sep.join([finalFolderPath, baseFileName+'.dcm'])
                    #self.dicom2png(inputFilePath, outputFilePath)
                    shutil.copy2(inputFilePath, outputFilePath)
                elif len(images) > 1:
                    for idx in range(0, len(images)):
                        inputFilePath = os.path.sep.join([currentFolderPath, images[idx]])
                        outputFilePath = os.path.sep.join([finalFolderPath, baseFileName + '_' + str(idx) +'.dcm'])
                        #self.dicom2png(inputFilePath, outputFilePath)
                        shutil.copy2(inputFilePath, outputFilePath)
 
    def copyDDSMFilesFullView(self, inputpath, outputpath, to_png=False):
        if not os.path.exists(outputpath):
            os.mkdir(outputpath)

        for folder in os.listdir(inputpath):
            print('folder: {}'.format(folder))
            baseFileName = '_'.join(folder.split('-')[1].split('_')[1:])
            if not folder.split('_')[-1].isnumeric():
                for currentFolderPath, subFolders, images in os.walk(os.path.sep.join([inputpath, folder])):
                    if images:
                        for idx in range(0, len(images)):
                            inputFilePath = os.path.sep.join([currentFolderPath, images[idx]])
                            if to_png:
                                outputFilePath = os.path.sep.join([outputpath, baseFileName + '_' + images[idx].split('.')[0] + '.png'])
                                self.dicom2png(inputFilePath, outputFilePath)
                            else:
                                outputFilePath = os.path.sep.join([outputpath, baseFileName + '_' + images[idx]])
                                shutil.copy2(inputFilePath, outputFilePath)
                                              
    def imageCropfromMaskLibra(self, imagepath, maskpath, outputimagepath):
        img = cv2.imread(imagepath)
        mask = cv2.imread(imagepath)
        mask_out=cv2.subtract(mask,img)
        final_img=cv2.subtract(mask,mask_out)
        #rect = cv2.boundingRect( cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY) )
        #final_cropped_img = final_img[y:y+h, x:x+w] #y=height and x=width
        #final_cropped_img = final_img[rect[0]:(rect[0]+rect[2]), rect[1]:(rect[1]+rect[3])]
        cv2.imwrite(outputimagepath, final_img)

    def imageCropfromMaskPanorama(self, imagepath, maskpath, outputCroppedImagepath, outputBwMaskPath, 
                                    winsize=512, winstep=512, thresh=127, mask_limit=.3):
        img = cv2.imread(imagepath)
        mask = cv2.imread(maskpath)
        imagefilename = os.path.basename(imagepath)
        thresh,blackWhiteMask = cv2.threshold(mask,127,255,0)
        #blackWhiteMask = blackWhiteMask[:,:,0]
        cv2.imwrite(outputBwMaskPath, blackWhiteMask)
        height, width = blackWhiteMask.shape[:2]
        croppedImage = np.zeros(shape=[height, width, 3], dtype=np.uint8)
        total_pixels = winsize*winsize
        for vindex in range(0, height - winsize, winstep):
            vert = height - winsize
            print("Image windowing process: {}%".format(round((100/(vert))*vindex,2)))
            for hindex in range(0, width - winsize, winstep):
                maskwindow = blackWhiteMask[vindex:vindex + winsize, hindex:hindex + winsize]
                imgwindow = img[vindex:vindex + winsize, hindex:hindex + winsize]
                pixels, num_pixels = np.unique(maskwindow, return_counts=True)
                if 255 in pixels:
                    #print('pixels', pixels)
                    #print('num_pixels', num_pixels)
                    num_mask_pixels = num_pixels[1]/3 if len(pixels) == 2 else num_pixels[0]/3
                    #print('num_mask_pixels', num_mask_pixels)
                    #print('total_pixels', total_pixels)
                    #print('limiar', total_pixels*mask_limit)
                    if num_mask_pixels > total_pixels*mask_limit:
                        croppedImage[vindex:vindex + winsize, hindex:hindex + winsize] = imgwindow
        cv2.imwrite(outputCroppedImagepath, croppedImage)

    def check_image_orientation(self, image):
        img_orientation = None
        if isinstance(image, str):
            img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
        else:
            img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        img_sum = np.sum(img, axis=0)
        #img_mean = int(np.mean(img_sum))
        img_mean = int(len(img_sum)/2)
        left_sum = np.sum(img_sum[0:img_mean])
        right_sum = np.sum(img_sum[img_mean::])
        if left_sum > right_sum:
            img_orientation = 'left'
        else:
            img_orientation = 'right'
        return img_orientation

    def flip_image(self, image, new_pos='left'):
        flipped = False
        image_pos = self.check_image_orientation(image)
        if image_pos != new_pos:
            image = cv2.flip(image, 1)
            flipped = True
        return image, flipped

    def calculate_class_weights(self, labelpath, labels):
        freqs = self.class_frequencies(labelpath, labels)
        total = sum(freqs.values())
        class_weigths = dict(freqs.items())
        for key in class_weigths:
            class_weigths[key] = 1 - class_weigths[key]/total
        return class_weigths

    def class_frequencies(self, labelpath, labels):
        num_labels = len(labels.items())
        class_frequencies = dict.fromkeys(list(range(0, num_labels)),0)
        for label in os.listdir(labelpath):
            label = cv2.imread(os.path.sep.join([labelpath, label]))
            unique, counts = np.unique(label, return_counts=True)
            for idx in range(len(unique)):
                class_frequencies[unique[idx]] += counts[idx]/3
        return class_frequencies

    def color_augment_images(self, imagespath, labelpath, num_times=5):
        for filename in os.listdir(imagespath):
            images = []
            im = cv2.imread(os.path.sep.join([imagespath, filename]))
            label = cv2.imread(os.path.sep.join([labelpath, filename]))
            images.append(im)
            images = np.array(images)
            for i in range(1, num_times):
                seq = iaa.OneOf([
                    iaa.GaussianBlur(sigma=(0, 2.0)),
                    iaa.MotionBlur(k=15, angle=[-45, 45]),
                    iaa.GammaContrast((0.5, 2.0)),
                    iaa.HistogramEqualization(),
                    iaa.CLAHE(),
                    iaa.LinearContrast((0.5, 2.0), per_channel=0.5)
                ])
                aug_images = seq(images=images)
                output_name = os.path.splitext(filename)[0] + '_c' + str(i) + '.png'
                cv2.imwrite( os.path.sep.join([imagespath, output_name]), aug_images[0] )
                cv2.imwrite( os.path.sep.join([labelpath, output_name]), label )
    
    def geometric_augment_images(self, imagespath, labelpath, num_times=5):
        for filename in os.listdir(imagespath):
            images = []
            labels = []
            im = cv2.imread(os.path.sep.join([imagespath, filename]))
            label = cv2.imread(os.path.sep.join([labelpath, filename]))
            images.append(im)
            images = np.array(images)
            labels.append(label)
            labels = np.array(labels)
            for i in range(1, num_times):
                seq = iaa.OneOf([
                    iaa.Flipud(0.5),
                    iaa.Affine(rotate=(-45, 45),
                        order=[0, 1],
                        scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}
                    )
                ])
                aug_images = seq(images=images)
                aug_labels = seq(images=labels)
                output_name = os.path.splitext(filename)[0] + '_g' + str(i) + '.png'
                cv2.imwrite( os.path.sep.join([imagespath, output_name]), aug_images[0] )
                cv2.imwrite( os.path.sep.join([labelpath, output_name]), aug_labels[0] )

    def check_label_images_out_of_bound(self, labels_path, accept_labels):
        non_accept_files = []
        for filename in os.listdir(labels_path):
            label = cv2.imread(os.path.sep.join([labels_path, filename]))
            classes, counts = np.unique(label, return_counts=True)
            for cl in classes:
                if not cl in accept_labels:
                    print(filename)
                    non_accept_files.append(filename)
        print('Total: ', len(non_accept_files), non_accept_files)

    def color_and_geometric_augmentation(self, imagespath, labelpath, num_times=5):
        for filename in os.listdir(imagespath):
            images = []
            labels = []
            im = cv2.imread(os.path.sep.join([imagespath, filename]))
            label = cv2.imread(os.path.sep.join([labelpath, filename]),0) #grayscale
            images.append(im)
            images = np.array(images)
            labels.append(label)
            labels = np.array(labels)
            for i in range(1, num_times):
                color_seq = iaa.OneOf([
                    iaa.GaussianBlur(sigma=(0, 2.0)),
                    iaa.MotionBlur(k=15, angle=[-45, 45]),
                    iaa.GammaContrast((0.5, 2.0)),
                    iaa.HistogramEqualization(),
                    iaa.CLAHE(),
                    iaa.LinearContrast((0.5, 2.0), per_channel=0.5)
                ])
                geo_seq = iaa.OneOf([
                    iaa.Flipud(0.5),
                    iaa.Affine(rotate=(-45, 45),
                        order=[0, 1],
                        scale={"x": (0.8, 1.2), "y": (0.8, 1.2)}
                    )
                ])
                color_aug_images = color_seq(images=images)
                output_name = os.path.splitext(filename)[0] + '_c' + str(i) + '.png'
                cv2.imwrite( os.path.sep.join([imagespath, output_name]), color_aug_images[0] )
                cv2.imwrite( os.path.sep.join([labelpath, output_name]), label )

                #geo_aug_images = geo_seq(images=images)
                #geo_aug_labels = geo_seq(images=labels)
                geo_aug_images, geo_aug_labels = geo_seq(images=images, segmentation_maps=labels)
                output_name = os.path.splitext(filename)[0] + '_g' + str(i) + '.png'
                cv2.imwrite( os.path.sep.join([imagespath, output_name]), geo_aug_images[0] )
                cv2.imwrite( os.path.sep.join([labelpath, output_name]), geo_aug_labels[0] )

    """ def smoothPredictedImage(self, imagePath, imageOut, origImage, winSize=112, stepsize=56, thres=0.9):
        img = cv2.imread(imagePath)
        origImage = cv2.imread(origImage)
        height, width = img.shape[:2]
        imagefilename = os.path.basename(imagePath)
        smoothedImage = np.zeros(shape=[height, width, 3], dtype=np.uint8)
        vert = height - winSize
        for vindex in range(0, height - winSize, stepsize):
            print("Image smoothing process: {}%".format(round((100/(vert))*vindex,2)))
            for hindex in range(0, width - winSize, stepsize):
                imgWindow = img[vindex:vindex + winSize, hindex:hindex + winSize]
                smoothedImgWindow = self.smoothImage(imgWindow, thres)
                smoothedImage[vindex:vindex + winSize, hindex:hindex + winSize] = smoothedImgWindow
        cv2.imwrite(imageOut, smoothedImage)

        intersection = np.logical_and(origImage, smoothedImage)
        union = np.logical_or(origImage, smoothedImage)
        iou_score = np.sum(intersection) / np.sum(union)
        print('Iou-Score: ', iou_score) """

    def smoothByMorph(self, prediction):
        kernel = np.ones((5,5), np.uint8)
        closing = cv2.morphologyEx(prediction.astype('uint8'), cv2.MORPH_CLOSE, kernel)
        return closing

    def smoothPrediction(self, prediction, winsize=7, stepsize=7, thres=0.9):
        origheight, origwidth = prediction.shape[:2]
        resized_prediction = cv2.resize(prediction, dsize=(10*origwidth, 10*origheight), interpolation=cv2.INTER_NEAREST)
        height, width = resized_prediction.shape[:2]
        smoothedPrediction = np.zeros(shape=[height, width], dtype=np.uint8)
        for vindex in range(0, height - winsize, stepsize):
            for hindex in range(0, width - winsize, stepsize):
                predwin = resized_prediction[vindex:vindex + winsize, hindex:hindex + winsize]
                smoothedPredWin = self.smoothImage(predwin, thres)
                smoothedPrediction[vindex:vindex + winsize, hindex:hindex + winsize] = smoothedPredWin
        smoothedPrediction = cv2.resize(smoothedPrediction, dsize=(origwidth, origheight), interpolation=cv2.INTER_NEAREST)        
        return smoothedPrediction

    def smoothImage(self, image, thres=0.9):
        height, width = image.shape[:2]
        px_thres = height*width*(1-thres)
        #print('px_thres', px_thres)
        labels,counts = np.unique(image, return_counts=True)
        #print('labels', labels)
        #print('counts', counts)
        max_label = labels[ np.argmax(counts) ]
        for idx in range(0, len(labels)):
            if counts[idx] < px_thres:
                #print('Change ', labels[idx], 'to ', max_label)
                image[image == labels[idx]] = max_label
        return image

    def segmentBreast(self, imagePath, segPath, outPath, breast_label=(82, 82, 146)):
        
        img = cv2.imread(imagePath)
        seg = cv2.imread(segPath)

        seg[np.all(seg != breast_label[::-1], axis=-1)] = (0,0,0)
        seg[np.all(seg == breast_label[::-1], axis=-1)] = (1,1,1)
        seg_breast = img*seg
        seg_breast_name = 'seg_breast_' + os.path.basename(imagePath)
        cv2.imwrite(seg_breast_name, seg_breast)

    def segmentByMask(self, imagesPath, segsPath, outPath):

        if not os.path.exists(outPath):
            os.makedirs(outPath)

        for segname in os.listdir(segsPath):
            print('image: ', segname)
            seg  = cv2.imread(os.path.sep.join([segsPath, segname]))
            img = cv2.imread(os.path.sep.join([imagesPath, segname.split('_')[1]]))
            alpha = 0.6
            masked_image = cv2.addWeighted(img, alpha, seg, 1-alpha, 0.0)
            cv2.imwrite(os.path.sep.join([outPath, segname]), masked_image)

    def segmentByContourv2(self, imagesPath, segsPath, labelDict, outPath, resize_to=1024, is_prediction=False):

        if not os.path.exists(outPath):
            os.makedirs(outPath)

        for segname in os.listdir(segsPath):
            print('image: ', segname)
            seg  = cv2.imread(os.path.sep.join([segsPath, segname]), cv2.IMREAD_GRAYSCALE)
            height, width = seg.shape[:2]
            if resize_to:
                seg = cv2.resize(seg, dsize=( resize_to, resize_to ), interpolation=cv2.INTER_NEAREST)

            if is_prediction:
                segname = segname.split('predseg_')[1]

            img = cv2.imread(os.path.sep.join([imagesPath, segname]),cv2.IMREAD_GRAYSCALE)
            img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
            if resize_to:
                img = cv2.resize(img, dsize=( resize_to, resize_to ), interpolation=cv2.INTER_NEAREST)

            #height, width = seg.shape[:2]

            edged = cv2.Canny(seg, 30, 200) 
            cv2.waitKey(0)

            contours,_ = cv2.findContours(edged,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

            for i,c in enumerate(contours):
                mask = np.zeros(seg.shape, np.uint8)
                cv2.drawContours(mask,c,-1,255, -1)
                mean,_,_,_ = cv2.mean(seg, mask=mask)
                label = 2 if mean > 100.0 else 1
                colour = labelDict.get(label)
                cv2.drawContours(img,c,-1,colour[::-1],10,cv2.LINE_AA)
        
            cv2.imwrite(os.path.sep.join([outPath, segname]), img)

    def segmentByContour(self, imagesPath, segsPath, expectPath, labelDict, labelExpected, outPath):

        if not os.path.exists(outPath):
            os.makedirs(outPath)

        for segname in os.listdir(segsPath):
            print('image: ', segname)
            seg  = cv2.imread(os.path.sep.join([segsPath, segname]), cv2.IMREAD_GRAYSCALE)
            img = cv2.imread(os.path.sep.join([imagesPath, segname.split('predseg_')[1]]),cv2.IMREAD_GRAYSCALE)
            img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)

            height, width = seg.shape[:2]

            expected_name = segname.split('predseg_')[1]

            exp = cv2.imread(os.path.sep.join([expectPath, expected_name]), cv2.IMREAD_GRAYSCALE)
            exp = cv2.resize(exp, dsize=(width, height), interpolation=cv2.INTER_NEAREST)

            edged = cv2.Canny(seg, 30, 200) 
            cv2.waitKey(0)

            edged_exp = cv2.Canny(exp, 30, 200) 
            cv2.waitKey(0)

            contours,_ = cv2.findContours(edged,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
            contours_exp,_ = cv2.findContours(edged_exp,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

            for i,c in enumerate(contours):
                mask = np.zeros(seg.shape, np.uint8)
                cv2.drawContours(mask,c,-1,255, -1)
                mean,_,_,_ = cv2.mean(seg, mask=mask)
                label = 2 if mean > 100.0 else 1
                colour = labelDict.get(label)
                cv2.drawContours(img,c,-1,colour[::-1],2,cv2.LINE_AA)

            for i,c in enumerate(contours_exp):
                mask = np.zeros(exp.shape, np.uint8)
                cv2.drawContours(mask,c,-1,255, -1)
                mean,_,_,_ = cv2.mean(exp, mask=mask)
                label = 2 if mean > 100.0 else 1
                colour = labelExpected.get(label)
                cv2.drawContours(img,c,-1,colour[::-1],2,cv2.LINE_AA)
        
            cv2.imwrite(os.path.sep.join([outPath, segname]), img)

    #https://stackoverflow.com/questions/60033274/how-to-remove-small-object-in-image-with-python
    #https://stackoverflow.com/questions/42798659/how-to-remove-small-connected-objects-using-opencv/42812226 (currently)
    def remove_small_objects(self, image, bound_area=150, connectivity=8):
        nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(image, connectivity=connectivity)
        sizes = stats[1:, -1] 
        nb_components = nb_components - 1
        result = np.zeros((output.shape))
        for i in range(0, nb_components):
            if sizes[i] >= bound_area:
                #result[output == i + 1] = 255
                result[output == i + 1] = 1
        return result

    def labelmelist(self, x, y):
        points = []
        for pxy in list(zip(x,y)):
            points.append(list(pxy))
        return points

    """ def mean_losses(self, val_losses):
        mean_val_losses = []
        for st in val_losses:
            val_losses.append(val_loss)
        return np.mean(np.array(val_losses), axis=0) """

    def plottraininghistory(self, histories, errors, titles, plotfilepath, colormap=None):
        errors_minus = histories-errors
        errors_plus = histories+errors
        if not colormap:
            colormap=['b', 'g', 'r', 'c', 'm', 'y', 'k']
        plt.style.use('ggplot')
        plt.figure()
        for idx in range(0, len(histories)):
            epochs = len(histories[idx])
            x = list(range(epochs))
            plt.plot(x, list(histories[idx]), label=titles[idx], color=colormap[idx])
            plt.fill_between(x, list(errors_minus[idx]), list(errors_plus[idx]), alpha=0.5, edgecolor=colormap[idx], facecolor=colormap[idx])
        plt.title('Validation Loss')
        plt.xlabel('Epoch #')
        plt.ylabel('Loss')
        plt.legend(loc='upper right')
        plt.savefig(plotfilepath)

    def plottrainingtime(self, times, metrics, titles, metric_name, plotfilepath):
        fig, ax1 = plt.subplots()

        ax1.set_xlabel('Models')
        ax1.set_ylabel('Time(s)')
        ax1.bar(titles, times, width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
        ax1.legend(loc="upper left")

        ax2 = ax1.twinx()
        ax2.yaxis.set_label_position("right")
        ax2.set_ylabel(metric_name)
        ax2.bar(titles, metrics, width=0.2, edgecolor='r', facecolor='r', label=metric_name)
        ax2.yaxis.tick_right()
        plt.setp(ax1.xaxis.get_majorticklabels(), rotation=45)
        ax2.legend(loc="upper right")

        plt.title('Trainning Time vs ' + metric_name)
        #plt.legend([ax1, ax2], ['Time', 'Loss'])
        #plt.legend()
        fig.tight_layout()
        plt.savefig(plotfilepath)

    def plottrainingtime4(self, data, titles, metric_name, plotfilepath):
        fig = plt.figure(figsize=(10, 5))
        outer = gridspec.GridSpec(1, 2, wspace=0.2, hspace=0.2)

        inner1 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[0], wspace=0.2, hspace=0.2)
        ax1 = plt.Subplot(fig, inner1[0])
        ax1.set_ylabel('Time(s)')
        ax1.set_ylim([0, 15000])
        ax1.bar(titles, data[0][0], width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
        ax1.legend(loc="upper left")
        ax2 = ax1.twinx()
        ax2.yaxis.set_label_position("right")
        ax2.set_ylabel(metric_name)
        ax2.bar(titles, data[0][1], width=0.2, edgecolor='r', facecolor='r', label=metric_name)
        ax2.yaxis.tick_right()
        plt.setp(ax1.xaxis.get_majorticklabels(), rotation=45)
        ax2.legend(loc="upper right")
        ax2.set_title('Before Optimization')

        inner2 = gridspec.GridSpecFromSubplotSpec(1, 1, subplot_spec=outer[1], wspace=0.2, hspace=0.2)
        ax3 = plt.Subplot(fig, inner2[0])
        ax3.set_ylabel('Time(s)')
        ax3.set_ylim([0, 15000])
        ax3.bar(titles, data[0][2], width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
        ax3.legend(loc="upper left")
        ax4 = ax3.twinx()
        ax4.yaxis.set_label_position("right")
        ax4.set_ylabel(metric_name)
        ax4.bar(titles, data[0][3], width=0.2, edgecolor='r', facecolor='r', label=metric_name)
        ax4.yaxis.tick_right()
        plt.setp(ax3.xaxis.get_majorticklabels(), rotation=45)
        ax4.legend(loc="upper right")
        ax4.set_title('After Optimization')
        
        plt.savefig(plotfilepath)

    #https://stackoverflow.com/questions/34933905/matplotlib-adding-subplots-to-a-subplot
    def plottrainingtime3(self, data, titles, metric_name, plotfilepath):
        fig = plt.figure(figsize=(10, 5))
        outer = gridspec.GridSpec(3, 1, wspace=0.2, hspace=0.2)

        for i in range(len(data)):
            inner = gridspec.GridSpecFromSubplotSpec(1, 2, subplot_spec=outer[i], wspace=0.2, hspace=0.2)
            ax1 = plt.Subplot(fig, inner[0])
            ax1.set_ylabel('Time(s)')
            ax1.set_ylim([0, 15000])
            ax1.bar(titles, data[i][0], width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
            ax1.legend(loc="upper left")
            ax2 = ax1.twinx()
            ax2.yaxis.set_label_position("right")
            ax2.set_ylabel(metric_name)
            ax2.bar(titles, data[i][1], width=0.2, edgecolor='r', facecolor='r', label=metric_name)
            ax2.yaxis.tick_right()
            plt.setp(ax1.xaxis.get_majorticklabels(), rotation=45)
            ax2.legend(loc="upper right")
            ax2.set_title('Before Optimization')

            ax3 = plt.Subplot(fig, inner[1])
            ax3.set_ylabel('Time(s)')
            ax3.set_ylim([0, 15000])
            ax3.bar(titles, data[i][2], width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
            ax3.legend(loc="upper left")
            ax4 = ax3.twinx()
            ax4.yaxis.set_label_position("right")
            ax4.set_ylabel(metric_name)
            ax4.bar(titles, data[i][3], width=0.2, edgecolor='r', facecolor='r', label=metric_name)
            ax4.yaxis.tick_right()
            plt.setp(ax3.xaxis.get_majorticklabels(), rotation=45)
            ax4.legend(loc="upper right")
            ax4.set_title('After Optimization')

        fig.tight_layout()
        plt.savefig(plotfilepath)

    def plottrainingtime5(self, data, titles, metric_name, plotfilepath):
        fig = plt.figure(figsize=(10, 10))
        count=1
        for i in range(3):
            for j in range(0,4,2):
                ax1 = fig.add_subplot(3, 2, count)
                ax1.set_ylabel('Time(s)')
                ax1.set_ylim([0, 15000])
                ax1.bar(titles, data[i][j], width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
                ax1.legend(loc="upper left")
                ax2 = ax1.twinx()
                ax2.yaxis.set_label_position("right")
                ax2.set_ylabel(metric_name)
                ax2.bar(titles, data[i][j+1], width=0.2, edgecolor='r', facecolor='r', label=metric_name)
                ax2.yaxis.tick_right()
                plt.setp(ax1.xaxis.get_majorticklabels(), rotation=45)
                ax2.legend(loc="upper right")
                if count % 2 == 0:
                    ax2.set_title('After Optimization')
                else:
                    ax2.set_title('Before Optimization')
                count+=1
        fig.tight_layout()
        plt.savefig(plotfilepath)

    def plottrainingtime2(self, times_before, times_after, metrics_before, metrics_after, titles, metric_name, plotfilepath):
        fig = plt.figure(figsize=(10, 5))

        ax1 = fig.add_subplot(1, 2, 1)

        #ax1.set_xlabel('Models')
        ax1.set_ylabel('Time(s)')

        #Try to change the limits to make tha same for the two bar graphs in time scale!
        #low = min(times_after)
        #high = max(times_after)
        ax1.set_ylim([0, 15000])

        ax1.bar(titles, times_before, width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
        ax1.legend(loc="upper left")

        ax2 = ax1.twinx()
        ax2.yaxis.set_label_position("right")
        ax2.set_ylabel(metric_name)
        ax2.bar(titles, metrics_before, width=0.2, edgecolor='r', facecolor='r', label=metric_name)
        ax2.yaxis.tick_right()
        plt.setp(ax1.xaxis.get_majorticklabels(), rotation=45)
        ax2.legend(loc="upper right")
        ax2.set_title('Before Optimization')

        ax3 = fig.add_subplot(1, 2, 2)
        #ax3.set_xlabel('Models')
        ax3.set_ylabel('Time(s)')

        ax3.set_ylim([0, 15000])

        ax3.bar(titles, times_after, width=0.4, edgecolor='g', facecolor='g', alpha=0.5, label='Time')
        ax3.legend(loc="upper left")

        ax4 = ax3.twinx()
        ax4.yaxis.set_label_position("right")
        ax4.set_ylabel(metric_name)
        ax4.bar(titles, metrics_after, width=0.2, edgecolor='r', facecolor='r', label=metric_name)
        ax4.yaxis.tick_right()
        plt.setp(ax3.xaxis.get_majorticklabels(), rotation=45)
        ax4.legend(loc="upper right")
        ax4.set_title('After Optimization')

        #plt.title('Trainning Time vs ' + metric_name)
        #plt.legend([ax1, ax2], ['Time', 'Loss'])
        #plt.legend()
        fig.tight_layout()
        plt.savefig(plotfilepath)

    def format_matrix(self, matrix):
        max_cols = len(max(matrix, key=len))
        extended_matrix = []
        for row in matrix:
            row = list(row)
            extra_cols = max_cols-len(row)
            row.extend( [row[-1]]*extra_cols )
            extended_matrix.append(row)
        return extended_matrix

    def multi_plot_predictions(self, datasets, models, images, model_nicks, resize_to=1024, outputfilepath='contours.png'):
        base_path = config.DIRPATH
        fig=plt.figure(figsize = (10,10), dpi=150)
        rot=60
        count = 1
        for idx in range(0, len(datasets)):
            dataset = datasets[idx]
            image_file_path = os.path.sep.join([base_path, images[idx]])
            image_name = os.path.basename(image_file_path)
            print('image', image_file_path)
            orig_img = cv2.imread(image_file_path)

            if dataset in ['INBREAST','MIAS_INBREAST']:
                orig_img = cv2.resize(orig_img, dsize=( resize_to, resize_to ), interpolation=cv2.INTER_NEAREST)

            images_folder = os.path.dirname(image_file_path)
            contour_path = os.path.sep.join([ str(Path(images_folder).parents[0]), 'contours' ])
            contour_file_path = os.path.sep.join([contour_path, image_name])
            print('contour', contour_file_path)
            orig_contour = cv2.imread( contour_file_path )
            
            ax = fig.add_subplot(len(datasets), len(models)+2, count)
            if idx == 0:
                ax.set_title('Image', rotation = rot)
            if dataset == 'MIAS_INBREAST':
                #ax.set_ylabel('MIAS + INBREAST')
                ax.set_ylabel('MI + IN')
            else:
                ax.set_ylabel(dataset)
            ax.axes.xaxis.set_ticks([])
            ax.axes.yaxis.set_ticks([])
                
            plt.imshow(orig_img)
            
            count += 1
            ax = fig.add_subplot(len(datasets), len(models)+2, count)
            if idx == 0:
                ax.set_title('Manual', rotation = rot)
            ax.axes.xaxis.set_ticks([])
            ax.axes.yaxis.set_ticks([])
            
            plt.imshow(orig_contour)

            for idy in range(0, len(models)):
                model = models[idy]
                nick = model_nicks[idy]
                contour_path = os.path.sep.join([ base_path, 'PREDICTIONS', dataset, model, 'contours' ])
                contour_file_path = os.path.sep.join([contour_path, image_name])
                print('contour', contour_file_path)
                contour = cv2.imread( contour_file_path )

                count += 1
                ax = fig.add_subplot(len(datasets), len(models)+2, count)
                if idx == 0:
                    ax.set_title(nick, rotation = rot, loc='right')
                
                ax.axes.xaxis.set_ticks([])
                ax.axes.yaxis.set_ticks([])
                plt.imshow(contour)

            count += 1
        plt.subplots_adjust(wspace=0.05, hspace=-0.85, top=0.95, bottom=0.01)
        plt.savefig(outputfilepath, bbox_inches = 'tight')

    def denoise(self, imagesPath, outsPath, apply_morph=True, apply_medianf=False, use_otsu=False, apply_clahe=False):
        if not os.path.exists(outsPath):
            os.makedirs(outsPath)
        for imgname in tqdm(os.listdir(imagesPath), desc='denoise processing rate:', position=0, 
                leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
            imagePath = os.path.sep.join([imagesPath, imgname])
            gray_img = cv2.imread(imagePath, cv2.COLOR_BGR2GRAY)
            resize_rate = 0.4
            height, width = gray_img.shape[:2]
            new_height = round(resize_rate*height)
            new_width = round(resize_rate*width)
            gray_img_resized = cv2.resize(gray_img, (new_height, new_width))
            #if apply_clahe:
            #    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
            #    gray_img_resized = clahe.apply(gray_img_resized)
            gray_img_resized_uint8 = cv2.convertScaleAbs(gray_img_resized)
            if use_otsu:
                (thresh, blackAndWhiteImage) = cv2.threshold(gray_img_resized_uint8, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
            else:
                (thresh, blackAndWhiteImage) = cv2.threshold(gray_img_resized_uint8, 16, 255, cv2.THRESH_BINARY)
            nlabels, labels, stats, centroids = cv2.connectedComponentsWithStats(blackAndWhiteImage, None, None, None, 8, cv2.CV_32S)
            areas = stats[1:,cv2.CC_STAT_AREA]
            result = np.zeros((labels.shape), np.uint8)
            for i in range(0, nlabels - 1):
                if areas[i] >= 3000:   #keep
                    result[labels == i + 1] = 1
            if apply_morph:
                kernel = np.ones((5,5),np.uint8)
                result = cv2.erode(result, kernel, iterations=1)
                result = cv2.dilate(result, kernel, iterations=1) 
            gray_img_resized = gray_img_resized*result
            gray_img_processed = cv2.resize(gray_img_resized, (width, height), interpolation=cv2.INTER_NEAREST)
            if apply_medianf:
                gray_img_processed = cv2.medianBlur(gray_img_processed,5)
            if apply_clahe:
                clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
                gray_img_processed = clahe.apply(gray_img_processed)
            cv2.imwrite(os.path.sep.join([outsPath, imgname]), gray_img_processed)

    def create_default_mask_from_cnn(self, dataset_path, modelpath, labels, chest_mask_points, pectoral_mask_points, breast_color, pectoral_color, start_idx):
        model = model_from_checkpoint_path(modelpath, True)
        output_width = model.output_width
        output_height  = model.output_height
        input_width = model.input_width
        input_height = model.input_height
        n_classes = model.n_classes

        image_files = [file for file in os.listdir(dataset_path) if os.path.splitext(file)[1] == '.png']
        start_idx = start_idx
        #for imgname in tqdm(image_files, desc='segmentation rate:', position=0, 
        #        leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
        for idx in tqdm(range(start_idx, len(image_files)), desc='segmentation rate:', position=0, 
                leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
            imgname = image_files[idx]
            imgpath = os.path.sep.join([dataset_path, imgname])
            img = cv2.imread(imgpath)
            height, width = img.shape[:2]
            img, flipped = self.flip_image(img, 'left')
            imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)
            pr = model.predict(np.array([imgwindow_arr]))[0]
            pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )
            seg_img = np.zeros((output_height, output_width, 3))
            for c in list(labels.values()):
                color = list(labels.keys())[list(labels.values()).index(c)]
                seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
                seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
                seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')
            if flipped:
                seg_img = cv2.flip(seg_img, 1)
            seg_img = cv2.resize(seg_img, (width, height), interpolation=cv2.INTER_NEAREST)
            seg_img = seg_img.astype(np.uint8)
            seg_img_chest = seg_img.copy()
            seg_img_pectoral = seg_img.copy()
            seg_img_chest[np.all(seg_img_chest != breast_color[::-1], axis=-1)] = (0,0,0)
            seg_img_pectoral[np.all(seg_img_pectoral != pectoral_color[::-1], axis=-1)] = (0,0,0)
            gray_chest = cv2.cvtColor(seg_img_chest, cv2.COLOR_BGR2GRAY)
            gray_pectoral = cv2.cvtColor(seg_img_pectoral, cv2.COLOR_BGR2GRAY)
            clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
            gray_chest = clahe.apply(gray_chest)
            gray_pectoral = clahe.apply(gray_pectoral)
            gray_chest = cv2.convertScaleAbs(gray_chest)
            gray_pectoral = cv2.convertScaleAbs(gray_pectoral)
            (_, bW_chest) = cv2.threshold(gray_chest, 16, 255, cv2.THRESH_BINARY)
            (_, bW_pectoral) = cv2.threshold(gray_pectoral, 16, 255, cv2.THRESH_BINARY)
            contours_chest,_ = cv2.findContours(bW_chest,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
            contours_pectoral,_ = cv2.findContours(bW_pectoral,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
            chest_contour_found = False
            pectoral_contour_found = False
            if len(contours_chest) > 0:
                print('Find Chest Contour')
                chest_contour_found=True
            if len(contours_pectoral) > 0:
                print('Find Pectoral Contour')
                pectoral_contour_found=True if imgname.find('_CC_') < 0 else False
                #print('pectoral_contour_found ', pectoral_contour_found)
                #print('imgname', imgname)
            if chest_contour_found and pectoral_contour_found:
                default_json = config.DIRPATH + '/default_MLO.json'
            else:
                default_json = config.DIRPATH + '/default_CC.json'

            with open(default_json) as f:
                data = json.load(f)
            encoded = base64.b64encode(open(imgpath, "rb").read()).decode()
            data['imageData'] = encoded
            data['imagePath'] = imgpath

            if chest_contour_found:
                x = contours_chest[0][:,0][:,1]
                y = contours_chest[0][:,0][:,0]
                n = len(x)

                idx = np.round(np.linspace(1,n,chest_mask_points+1))
                idx = idx[:-1]
                idx = idx.astype(int)
                x = x[idx]
                y = y[idx]

                data["shapes"][0]["label"] = 'MAMA'
                data["shapes"][0]["points"] = self.labelmelist(y.tolist(),x.tolist())

            if pectoral_contour_found and chest_contour_found:
                x = contours_pectoral[0][:,0][:,1]
                y = contours_pectoral[0][:,0][:,0]
                n = len(x)

                idx = np.round(np.linspace(1,n,pectoral_mask_points+1))
                idx = idx[:-1]
                idx = idx.astype(int)
                x = x[idx]
                y = y[idx]

                data["shapes"][1]["label"] = 'PEITORAL'
                data["shapes"][1]["points"] = self.labelmelist(y.tolist(),x.tolist())

            #folderpath = config.DATASETS_PATH + dataset_path
            #json_file_name = folderpath + '/' + imgname + ".json"
            json_file_name = os.path.sep.join([dataset_path, imgname.split('.')[0] + ".json"])
            with open(json_file_name, 'w') as json_file:
                json.dump(data, json_file)
            os.system("labelme " + imgpath + " -O " + json_file_name)

    def seg_trainning(self, inputtrainpath, inputvalpath, outputtrainpath, outputvalpath):

        #os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        #os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

        #train_datagen = ImageDataGenerator(rescale=1./255,shear_range=0.2,zoom_range=0.2,horizontal_flip=True)

        #val_datagen = ImageDataGenerator(rescale=1./255)

        #train_image_generator = train_datagen.flow_from_directory(inputtrainpath, batch_size=8)
        #train_label_generator = train_datagen.flow_from_directory(outputtrainpath, batch_size=8)

        #val_image_generator = val_datagen.flow_from_directory(inputvalpath, batch_size=8)
        #val_label_generator = val_datagen.flow_from_directory(outputvalpath, batch_size=8)

        #train_generator = zip(train_image_generator, train_label_generator)
        #val_generator = zip(val_image_generator, val_label_generator)

        train_generator = self.data_gen(inputtrainpath, outputtrainpath, batch_size = 8)
        val_generator = self.data_gen(inputvalpath, outputvalpath, batch_size = 8)

        model = VGG16(include_top=True, input_shape=(224, 224, 3))
        model.compile(loss="categorical_crossentropy", optimizer='adadelta', metrics=["accuracy"])

        H = model.fit_generator(train_generator, 
                                epochs=10, 
                                steps_per_epoch=math.ceil(0.75*322/8), 
                                validation_data=val_generator, validation_steps=math.ceil((0.25*322/8)/2))

        m.save('Model.h5')

        print(H)