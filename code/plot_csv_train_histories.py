import pandas as pd
import numpy as np
from SegImages import SegImages
import os


f1 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_resnet50_unet_mobilenet_unet_dice_loss_dice_loss_adadelta_adadelta_accuracy_accuracy_none_none_inbreast_clw1_1smooth0_0.csv"
f2 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_vgg_unet_unet_dice_loss_dice_loss_adadelta_adadelta_accuracy_accuracy_none_none_inbreast_clw1_1smooth0_0.csv"
f3 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_segnet_dice_loss_adadelta_accuracy_none_inbreast_clw1smooth0.csv"

seg = SegImages()

val_losses = []
std_losses = []

df1 = pd.read_csv(f1)
val_losses.append(df1.iloc[:,0].values.T)
val_losses.append(df1.iloc[:,2].values.T)
std_losses.append(df1.iloc[:,1].values.T)
std_losses.append(df1.iloc[:,3].values.T)
c1 = list(df1.columns)

df2 = pd.read_csv(f2)
val_losses.append(df2.iloc[:,0].values.T)
val_losses.append(df2.iloc[:,2].values.T)
std_losses.append(df2.iloc[:,1].values.T)
std_losses.append(df2.iloc[:,3].values.T)
c2 = list(df2.columns)

df3 = pd.read_csv(f3)
val_losses.append(df3.iloc[:,0].values.T)
std_losses.append(df3.iloc[:,1].values.T)
c3 = list(df3.columns)

#c = [c1[0],c1[2],c2[0],c2[2],c3[0]] 
c = ['resnet50_unet', 'mobilenet_unet', 'vgg_unet', 'unet', 'segnet']
plotfilepath='D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_resnet50_mobilenet_vgg_unet_segnet_after_optimization_inbreast.png'

models_val_losses = seg.format_matrix(val_losses)
models_val_losses_std = seg.format_matrix(std_losses)

models_val_losses = np.array(models_val_losses)
models_val_losses_std = np.array(models_val_losses_std)

seg.plottraininghistory(models_val_losses, models_val_losses_std, c, plotfilepath)

