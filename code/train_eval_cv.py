from SegImages import SegImages
import os
import config


seg = SegImages()

IOU, STD = seg.seg_model_train_cross_valid(config.KFOLDS, config.CROSS_VALID_PATH, config.OUTPUTPATH, config.SEGMODEL, 
    config.BATCH_SIZE, config.LABELS, config.LABEL_NAMES, config.NUM_EPOCHS)

print(IOU)
print(STD)