import cv2
import numpy as np
import os


#image_path = 'C:\\Users\\rodri\\Downloads\\Imagens_teste-20200812T013022Z-001\\Imagens_teste\\2-ANP-2A-RJS_T2_CX5_19.PNG'
image_path = 'C:\\Users\\rodri\\Downloads\\3-RJS-744-RJ_TESTO01_CX8_70\\DSC_2163.JPG'
outputbluredImagepath = 'deblur_DSC_2163.JPG'
img = cv2.imread(image_path)
winsize = 56
winstep = 56
blur_limit = 20
sharpen_kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])

filename = os.path.basename(image_path)

height, width = img.shape[:2]
reblured_Image = np.zeros(shape=[height, width, 3], dtype=np.uint8)

for vindex in range(0, height - winsize, winstep):
    vert = height - winsize
    print("Image windowing process: {}%".format(round((100/(vert))*vindex,2)))
    for hindex in range(0, width - winsize, winstep):
        imgwindow = img[vindex:vindex + winsize, hindex:hindex + winsize]
        graywindow = cv2.cvtColor(imgwindow, cv2.COLOR_BGR2GRAY)
        blur = cv2.Laplacian(graywindow, cv2.CV_64F).var()
        #print(blur)
        if blur < blur_limit:
            imgwindow = cv2.filter2D(imgwindow, -1, sharpen_kernel)
            #print('filter windows!!!')
        reblured_Image[vindex:vindex + winsize, hindex:hindex + winsize] = imgwindow
cv2.imwrite(outputbluredImagepath, reblured_Image)