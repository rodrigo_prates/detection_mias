from keras_segmentation.predict import model_from_checkpoint_path
from keras_segmentation.data_utils.data_loader import *
from keras_segmentation.models.config import IMAGE_ORDERING
import config
import numpy as np
import cv2
from SegImages import SegImages
import base64
import json
import os


dataset_path = "/INBREAST_FULL_VIEW_PNG/images_ml"
imgpath = config.DATASETS_PATH + dataset_path + '/20586960_6c613a14b80a8591_MG_R_ML_ANON.png'
imgname = os.path.basename(imgpath).split('.')[0]
modelname = 'mobilenet_unet_none_win224_metric_accuracy_miasinbreast.h5'
modelpath = config.MODELS_PATH + '/mobilenet_unet/checkpoints/' + modelname
model = model_from_checkpoint_path(modelpath, True)
seg = SegImages()
output_width = model.output_width
output_height  = model.output_height
input_width = model.input_width
input_height = model.input_height
n_classes = model.n_classes

labels = config.NEWLABELS
chest_mask_points = 16
pectoral_mask_points = 6

img = cv2.imread(imgpath)
height, width = img.shape[:2]

img, flipped = seg.flip_image(img, 'left')

imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)
pr = model.predict(np.array([imgwindow_arr]))[0]
pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )

seg_img = np.zeros((output_height, output_width, 3))

for c in list(labels.values()):
    color = list(labels.keys())[list(labels.values()).index(c)]
    seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
    seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
    seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')

if flipped:
    seg_img = cv2.flip(seg_img, 1)

seg_img = cv2.resize(seg_img, (width, height), interpolation=cv2.INTER_NEAREST)

seg_img = seg_img.astype(np.uint8)

seg_img_chest = seg_img.copy()
seg_img_pectoral = seg_img.copy()
breast_color = (82, 82, 146)
pectoral_color = (212, 212, 148)

seg_img_chest[np.all(seg_img_chest != breast_color[::-1], axis=-1)] = (0,0,0)
seg_img_pectoral[np.all(seg_img_pectoral != pectoral_color[::-1], axis=-1)] = (0,0,0)

gray_chest = cv2.cvtColor(seg_img_chest, cv2.COLOR_BGR2GRAY)
gray_pectoral = cv2.cvtColor(seg_img_pectoral, cv2.COLOR_BGR2GRAY)

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

gray_chest = clahe.apply(gray_chest)
gray_pectoral = clahe.apply(gray_pectoral)

gray_chest = cv2.convertScaleAbs(gray_chest)
gray_pectoral = cv2.convertScaleAbs(gray_pectoral)

(_, bW_chest) = cv2.threshold(gray_chest, 16, 255, cv2.THRESH_BINARY)
(_, bW_pectoral) = cv2.threshold(gray_pectoral, 16, 255, cv2.THRESH_BINARY)

contours_chest,_ = cv2.findContours(bW_chest,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
contours_pectoral,_ = cv2.findContours(bW_pectoral,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

chest_contour_found = False
pectoral_contour_found = False
if len(contours_chest) > 0:
    chest_contour_found=True

if len(contours_pectoral) > 0:
    pectoral_contour_found=True

if chest_contour_found and pectoral_contour_found:
    default_json = config.DIRPATH + '/default_MLO.json'
else:
    default_json = config.DIRPATH + '/default_CC.json'

with open(default_json) as f:
  data = json.load(f)
encoded = base64.b64encode(open(imgpath, "rb").read()).decode()
data['imageData'] = encoded
data['imagePath'] = imgpath

if chest_contour_found:
    x = contours_chest[0][:,0][:,1]
    y = contours_chest[0][:,0][:,0]
    n = len(x)

    idx = np.round(np.linspace(1,n,chest_mask_points+1))
    idx = idx[:-1]
    idx = idx.astype(int)
    x = x[idx]
    y = y[idx]

    data["shapes"][0]["label"] = 'MAMA'
    data["shapes"][0]["points"] = seg.labelmelist(y.tolist(),x.tolist())

if pectoral_contour_found and chest_contour_found:
    x = contours_pectoral[0][:,0][:,1]
    y = contours_pectoral[0][:,0][:,0]
    n = len(x)

    idx = np.round(np.linspace(1,n,pectoral_mask_points+1))
    idx = idx[:-1]
    idx = idx.astype(int)
    x = x[idx]
    y = y[idx]

    data["shapes"][1]["label"] = 'PEITORAL'
    data["shapes"][1]["points"] = seg.labelmelist(y.tolist(),x.tolist())

folderpath = config.DATASETS_PATH + dataset_path
json_file_name = folderpath + '/' + imgname + ".json"
with open(json_file_name, 'w') as json_file:
    json.dump(data, json_file)
os.system("labelme " + imgpath + " -O " + json_file_name)