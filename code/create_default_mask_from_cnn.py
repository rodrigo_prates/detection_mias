from SegImages import SegImages
import config
import os


seg = SegImages()
dataset_path = os.path.sep.join([config.DATASETS_PATH, 'DDSM_FULL_VIEW_PNG/temp'])
modelname = 'mobilenet_unet_none_win224_metric_accuracy_loss_none_denoise_0_dataset_ddsm'
modelpath = config.MODELS_PATH + '/mobilenet_unet/checkpoints/' + modelname
labels = config.NEWLABELS
chest_mask_points = 19
pectoral_mask_points = 6
breast_color=(82, 82, 146)
pectoral_color=(212, 212, 148)
start_idx=0
seg.create_default_mask_from_cnn(dataset_path, modelpath, labels, chest_mask_points, pectoral_mask_points, breast_color, pectoral_color, start_idx)