#https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_morphological_ops/py_morphological_ops.html
import os
import cv2
import numpy as np


origImage = "D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\\MIAS\\mobilenet_unet\\predseg_mdb263.png"
img = cv2.imread(origImage)

kernel = np.ones((5,5), np.uint8) 

#Closing
closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
cv2.imwrite('D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\\closing_predseg_mdb263.png', closing)
