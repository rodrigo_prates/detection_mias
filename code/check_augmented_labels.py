import cv2
import numpy as np
import os


fold_path = "D:/DOUTORADO_2020/detection_mias/DATASETS/MIAS_INBREAST/kfolds/train0_output"
out_path = "D:/DOUTORADO_2020/detection_mias/DATASETS/MIAS_INBREAST/kfolds_validate"

if not os.path.exists(out_path):
    os.makedirs(out_path)

for label in os.listdir(fold_path):
    img = cv2.imread( os.path.sep.join([fold_path, label]) )
    print(np.unique(img,return_counts=True))
    img = img*100
    cv2.imwrite( os.path.sep.join([out_path, label]), img )
