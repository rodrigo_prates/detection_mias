import pandas as pd
import numpy as np
from SegImages import SegImages
import os


f1 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_resnet50_unet_mobilenet_unet_none_none_adadelta_adadelta_accuracy_accuracy_none_none_mias_clw0_0smooth0_0.csv"
f2 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_vgg_unet_unet_none_none_adadelta_adadelta_accuracy_accuracy_none_none_mias_clw0_0smooth0_0.csv"
f3 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_segnet_none_adadelta_accuracy_none_mias_clw0smooth0.csv"

f4 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\train_time_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.csv"

seg = SegImages()

mean_losses = []

df1 = pd.read_csv(f1)
mean_losses.append( np.mean(df1.iloc[:,0].values, axis=0) )
mean_losses.append( np.mean(df1.iloc[:,2].values, axis=0) )
c1 = list(df1.columns)

df2 = pd.read_csv(f2)
mean_losses.append( np.mean(df2.iloc[:,0].values, axis=0) )
mean_losses.append( np.mean(df2.iloc[:,2].values, axis=0) )
c2 = list(df2.columns)

df3 = pd.read_csv(f3)
mean_losses.append( np.mean(df3.iloc[:,0].values, axis=0) )
c3 = list(df3.columns)

#c = [c1[0],c1[2],c2[0],c2[2],c3[0]]
c = ['resnet50_unet', 'mobilenet_unet', 'vgg_unet', 'unet', 'segnet']

df4 = pd.read_csv(f4)
models_training_time = df4.iloc[0,1:].values.T

metric_name="Loss"

plotfilepath='D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\train_time_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.png' 
seg.plottrainingtime(models_training_time, mean_losses, c, metric_name, plotfilepath)