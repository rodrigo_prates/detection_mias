import pandas as pd
#import numpy as np
from SegImages import SegImages


f11 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIAS\\train_time_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.csv"
f12 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIAS\\jaccard_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.csv"
f13 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIAS\\train_time_resnet50_mobilenet_vgg_unet_segnet_after_optimization_mias.csv"
f14 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIAS\\jaccard_resnet50_mobilenet_vgg_unet_segnet_after_optimization_mias.csv"

f21 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\INBREAST\\train_time_resnet50_mobilenet_vgg_unet_segnet_before_optimization_inbreast.csv"
f22 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\INBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_before_optimization_inbreast.csv"
f23 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\INBREAST\\train_time_resnet50_mobilenet_vgg_unet_segnet_after_optimization_inbreast.csv"
f24 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\INBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_after_optimization_inbreast.csv"

f31 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\train_time_resnet50_mobilenet_vgg_unet_segnet_before_optimization_miasinbreast.csv"
f32 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_before_optimization_miasinbreast.csv"
f33 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\train_time_resnet50_mobilenet_vgg_unet_segnet_after_optimization_miasinbreast.csv"
f34 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_after_optimization_miasinbreast.csv"

fs = [
    [f11,f12,f13,f14],
    [f21,f22,f23,f24],
    [f31,f32,f33,f34]
]

df11 = pd.read_csv(f11)
df12 = pd.read_csv(f12)
df13 = pd.read_csv(f13)
df14 = pd.read_csv(f14)

df21 = pd.read_csv(f21)
df22 = pd.read_csv(f22)
df23 = pd.read_csv(f23)
df24 = pd.read_csv(f24)

df31 = pd.read_csv(f31)
df32 = pd.read_csv(f32)
df33 = pd.read_csv(f33)
df34 = pd.read_csv(f34)

mias_time_before = df11.iloc[0,1:].values.T
mias_jac_before = df12.mean(axis=0).values.T
mias_time_after = df13.iloc[0,1:].values.T
mias_jac_after = df14.mean(axis=0).values.T
mias_data=[mias_time_before,
    mias_jac_before,
    mias_time_after,
    mias_jac_after]

inbreast_time_before = df21.iloc[0,1:].values.T
inbreast_jac_before = df22.mean(axis=0).values.T
inbreast_time_after = df23.iloc[0,1:].values.T
inbreast_jac_after = df24.mean(axis=0).values.T
inbreast_data=[inbreast_time_before,
    inbreast_jac_before,
    inbreast_time_after,
    inbreast_jac_after]

miasinbreast_time_before = df31.iloc[0,1:].values.T
miasinbreast_jac_before = df32.mean(axis=0).values.T
miasinbreast_time_after = df33.iloc[0,1:].values.T
miasinbreast_jac_after = df34.mean(axis=0).values.T
miasinbreast_data=[miasinbreast_time_before,
    miasinbreast_jac_before,
    miasinbreast_time_after,
    miasinbreast_jac_after]

seg = SegImages()

c = ['Resnet50_Unet', 'Mobilenet_Unet', 'Vgg_Unet', 'Unet', 'Segnet']

metric_name = 'Jaccard'

plotfilepath='D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\train_time_resnet50_mobilenet_vgg_unet_segnet_all_data2.png'

data = [mias_data,inbreast_data,miasinbreast_data]
seg.plottrainingtime5(data,c,metric_name,plotfilepath)