import os
import cv2
import numpy as np
from SegImages import SegImages
import json
import base64
import config


seg = SegImages()
imgpath = config.DATASETS_PATH + '/MIAS_PNG/images/mdb002.png'
imgname = os.path.basename(imgpath).split('.')[0]
#img = cv2.imread(imgpath)
img = cv2.imread(imgpath, cv2.IMREAD_GRAYSCALE)
resize_rate = 0.3
height, width = img.shape[:2]
new_height = round(resize_rate*height)
new_width = round(resize_rate*width)
img_resize = cv2.resize(img, (new_height, new_width))
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (63,63))
opening = cv2.morphologyEx(img_resize, cv2.MORPH_OPEN, kernel)
img_morph = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
img_morph = (img_morph>2)*1
img_morph_orig = cv2.resize(img_morph, (height, width), interpolation=cv2.INTER_NEAREST)
img_morph_orig = img_morph_orig.astype(np.uint8)
area = np.unique(img_morph_orig, return_counts=True)[1][1]
img_morph_orig = seg.remove_small_objects(img_morph_orig, area, 8)
img_morph_orig = img_morph_orig.astype(np.uint8)
k = np.zeros((height, width)).astype(np.uint8)
a = round((height*0.025)/2)
b = round((width*0.025)/2)
#K(a:M-a+1,b:N-b+1) = 1;
k[a:height-a+1, b:width-b+1] = 1
#B(~K) = 0;
img_morph_orig[1-k] = 0
mask_points = 16
cnts = cv2.findContours(img_morph_orig, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) #cv2.RETR_TREE or cv2.RETR_EXTERNAL
#x = max(P{1}(:,2),1);
x = cnts[0][0][:,0][:,1]
y = cnts[0][0][:,0][:,0]
n = len(x)
#idx = round(linspace(1,n,np+1));
idx = np.round(np.linspace(1,n,mask_points+1))
idx = idx[:-1]
idx = idx.astype(int)
x = x[idx]
y = y[idx]

#[x,idx] = unique(x,'stable');
#_,idx = np.unique(x, return_index=True)
#y = y[idx]
#[y,idx] = unique(y,'stable');
#_,idx = np.unique(y, return_index=True)
#x = x[idx]

default_json = config.DIRPATH + '/default_json.json'
with open(default_json) as f:
  data = json.load(f)
encoded = base64.b64encode(open(imgpath, "rb").read()).decode()
data['imageData'] = encoded
data['imagePath'] = imgpath

data["shapes"][0]["label"] = 'MAMA'
data["shapes"][0]["points"] = seg.labelmelist(y.tolist(),x.tolist())

#print(data["shapes"][0]["points"])

folderpath = config.DATASETS_PATH + "/MIAS_PNG/MIAS/images"
json_file_name = folderpath + '/' + imgname + ".json"
with open(json_file_name, 'w') as json_file:
    json.dump(data, json_file)
os.system("labelme " + imgpath + " -O " + json_file_name)