import config
import os
from SegImages import SegImages


seg = SegImages()
imagesPath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_PNG\MIAS\\kfolds\\test0_input"
segsPath = "D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\MIAS\\mobilenet_unet\\images"
outPath = "D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\MIAS\\mobilenet_unet\\masked_images"
seg.segmentByMask(imagesPath, segsPath, outPath)