import os


path = 'D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_PNG\\MIAS\\newmasks'
for filename in os.listdir(path):
    new_filename = filename.split('_')[0] + '.png'
    old_filepath = os.path.sep.join([path, filename])
    new_filepath = os.path.sep.join([path, new_filename])
    os.rename(old_filepath, new_filepath)
