import config
import os
from SegImages import SegImages


seg = SegImages()
imagesPath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\INBREAST_FULL_VIEW_PNG\\images_ml"
segsPath = "D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\\INBREAST\\mlp\\images"
#expectPath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\INBREAST_FULL_VIEW_PNG\\masks"
labelDict = {0:(0, 64, 0), 1: (82, 82, 146), 2: (212, 212, 148)}
#labelExpected = {0:(255, 0, 0), 1: (255, 0, 0), 2: (255, 0, 0)}
outPath = "D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\\INBREAST\\mlp\\contours"

#seg.segmentByContour(imagesPath, segsPath, expectPath, labelDict, labelExpected, outPath)
resize_to = 1024 #Number or None
is_prediction=True
seg.segmentByContourv2(imagesPath, segsPath, labelDict, outPath, resize_to, is_prediction)