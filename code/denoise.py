import cv2
import config
import numpy as np
import os
from SegImages import SegImages


seg = SegImages()
imagesPath = config.DATASETS_PATH + '/MIAS_INBREAST/images'

outsPath = config.DATASETS_PATH + '/MIAS_INBREAST/images_denoise_clahe'

apply_morph=True
apply_medianf=False
use_otsu=False
apply_clahe=True

seg.denoise(imagesPath, outsPath, apply_morph, apply_medianf, use_otsu, apply_clahe)