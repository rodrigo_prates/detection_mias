import json
import csv
import os
import numpy as np


json_file_path = 'D:/DOUTORADO_2020/detection_mias/DATASETS/MIAS_PNG/MIAS/images/mdb001.json'
csv_file_path = 'mias.csv'
csv_headers = ['filename','label','x','y']

with open(json_file_path) as json_file: 
    data = json.load(json_file)

filename = os.path.basename(data['imagePath'])

if len(data["shapes"]) > 1:
    with open(csv_file_path, mode='w') as csv_file:
        #writer = csv.writer(csv_file, delimiter=',')
        #writer.writerow(csv_headers)
        #writer.writerow([filename, 'MAMA', list(np.array(data["shapes"][0]["points"])[:,0]), list(np.array(data["shapes"][0]["points"])[:,1])])
        #writer.writerow([filename, 'PEITORAL', list(np.array(data["shapes"][1]["points"])[:,0]), list(np.array(data["shapes"][1]["points"])[:,1])])
        writer = csv.DictWriter(csv_file, csv_headers)
        writer.writeheader()
        writer.writerow({'filename': filename, 'label': 'MAMA', 'x': list(np.array(data["shapes"][0]["points"])[:,0]), 'y': list(np.array(data["shapes"][0]["points"])[:,1])})
        writer.writerow({'filename': filename, 'label': 'PEITORAL', 'x': list(np.array(data["shapes"][1]["points"])[:,0]), 'y': list(np.array(data["shapes"][1]["points"])[:,1])})
else:
    with open(csv_file_path, mode='w') as csv_file:
        #writer = csv.writer(csv_file, delimiter=',')
        #writer.writerow(csv_headers)
        #writer.writerow([filename, 'MAMA', list(np.array(data["shapes"][0]["points"])[:,0]), list(np.array(data["shapes"][0]["points"])[:,1])])
        writer = csv.DictWriter(csv_file, csv_headers)
        writer.writeheader()
        writer.writerow({'filename': filename, 'label': 'MAMA', 'x': list(np.array(data["shapes"][0]["points"])[:,0]), 'y': list(np.array(data["shapes"][0]["points"])[:,1])})