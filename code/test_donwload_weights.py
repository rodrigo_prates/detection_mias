""" from __future__ import print_function
import pickle
import os.path
from googleapiclient import discovery
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import io


mobilenet_unet_url='https://drive.google.com/drive/folders/1hv6T4cDcEwogsACwMX_VV3shJInrd1_v?usp=sharing'
file_url='https://drive.google.com/file/d/1QVBZ9UeZpTpTG9h2c8dO-vpVlUO6kAsl/view?usp=sharing'
file_id='1QVBZ9UeZpTpTG9h2c8dO-vpVlUO6kAsl'

drive_service = discovery.build('drive', 'v3')
request = drive_service.files().get_media(fileId=file_id)
fh = io.BytesIO()
downloader = MediaIoBaseDownload(fh, request)
done = False
while done is False:
    status, done = downloader.next_chunk()
    print ("Download %d%%." % int(status.progress() * 100)) """


import requests, zipfile, io


zip_file_url='https://bitbucket.org/rodrigo_prates/detection_mias/src/create_documentation/best_models.zip'
r = requests.get(zip_file_url)
z = zipfile.ZipFile(io.BytesIO(r.content))
z.extractall("zip/")