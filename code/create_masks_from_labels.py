from SegImages import SegImages
import os
import config


seg = SegImages()
labelpath = config.MIAS_INBREAST_LABELPATH
maskpath = config.MIAS_INBREAST_MASKPATH
seg.label2mask(labelpath, config.NEWLABELS, maskpath)