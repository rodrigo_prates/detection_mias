import os
import cv2
import numpy as np
from tqdm import tqdm
from colorama import Fore
from keras_segmentation.predict import model_from_checkpoint_path
from keras_segmentation.data_utils.data_loader import *
from keras_segmentation.models.config import IMAGE_ORDERING
from SegImages import SegImages


modelpath = 'D:/DOUTORADO_2020/breast_density_classification/weights/mobilenet_unet/checkpoints/mobilenet_unet_just_breast_none_win224_metric_accuracy_loss_none_denoise_0_dataset_ddsm'
model = model_from_checkpoint_path(modelpath, True)
seg = SegImages()

output_width = model.output_width
output_height  = model.output_height
input_width = model.input_width
input_height = model.input_height
n_classes = model.n_classes

labels = {(0, 0, 0): 0, 
          (82, 82, 146): 1}

breast_label = (82, 82, 146)

output_cropped_path = 'D:/DOUTORADO_2020/breast_density_classification/all_dataset/test_path_cropped/2'
output_mask_path = 'D:/DOUTORADO_2020/breast_density_classification/all_dataset/test_path_masks/2'
output_seg_path = 'D:/DOUTORADO_2020/breast_density_classification/all_dataset/test_path_segmented/2'
alpha = 0.6

if not os.path.exists(output_cropped_path):
    os.makedirs(output_cropped_path)

if not os.path.exists(output_mask_path):
    os.makedirs(output_mask_path)

if not os.path.exists(output_seg_path):
    os.makedirs(output_seg_path)

inputtestpath = 'D:/DOUTORADO_2020/breast_density_classification/all_dataset/test_path/2'

for imgfile in tqdm(sorted(os.listdir(inputtestpath)), desc='Images', position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
    imgfilepath = os.path.sep.join([inputtestpath, imgfile])
    img = cv2.imread(imgfilepath)
    height, width = img.shape[:2]
    img, flipped = seg.flip_image(img, 'left')
    imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)
    pr = model.predict(np.array([imgwindow_arr]))[0]
    pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )
    seg_img = np.zeros((output_height, output_width, 3))

    for c in list(labels.values()):
        color = list(labels.keys())[list(labels.values()).index(c)]
        seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
        seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
        seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')

    if flipped:
        seg_img = cv2.flip(seg_img, 1)
        img = cv2.flip(img, 1)

    seg_img = cv2.resize(seg_img, (width, height), interpolation=cv2.INTER_NEAREST)
    seg_img = seg_img.astype('uint8')
    cv2.imwrite(os.path.sep.join([output_mask_path, imgfile]), seg_img)

    masked_image = cv2.addWeighted(img, alpha, seg_img, 1-alpha, 0.0)
    cv2.imwrite(os.path.sep.join([output_seg_path, imgfile]), masked_image)

    seg_img[np.all(seg_img != breast_label[::-1], axis=-1)] = (0,0,0)
    seg_img[np.all(seg_img == breast_label[::-1], axis=-1)] = (1,1,1)
    img_corpped = img*seg_img
    cv2.imwrite(os.path.sep.join([output_cropped_path, imgfile]), img_corpped)