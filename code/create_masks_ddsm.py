from SegImages import SegImages
import os
import config


seg = SegImages()
ddsm_full_view_path = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\DDSM_FULL_VIEW_PNG\\images"
csvpath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\DDSM_FULL_VIEW_PNG\\csv_contours.csv"
labels = config.NEWLABELS
label_names = config.NEW_LABEL_NAMES
outputpath_masks = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\DDSM_FULL_VIEW_PNG\\masks"
outputpath_labels = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\DDSM_FULL_VIEW_PNG\\labels"
default_background = "FUNDO_REAL"
seg.createMasksFromCoordsV2(ddsm_full_view_path, csvpath, labels, label_names, outputpath_masks, 
                                outputpath_labels, default_background)