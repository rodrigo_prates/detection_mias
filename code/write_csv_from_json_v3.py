import os
from tqdm import tqdm
import json
import csv
import numpy as np
from colorama import Fore


json_files_path = 'D:/DOUTORADO_2020/detection_mias/DATASETS/DDSM_FULL_VIEW_PNG/json_files'
csv_file_path = 'D:/DOUTORADO_2020/detection_mias/DATASETS/DDSM_FULL_VIEW_PNG/csv_contours.csv'
csv_headers = ['filename','label','x','y']

with open(csv_file_path, mode='w') as csv_file:
    writer = csv.DictWriter(csv_file, csv_headers)
    writer.writeheader()

    for json_filename in tqdm(os.listdir(json_files_path), desc='json_files:', position=0, 
                    leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
        if json_filename.endswith(".json"):
            json_file_path = os.path.sep.join([json_files_path, json_filename])
            with open(json_file_path) as json_file: 
                data = json.load(json_file)
            
            filename = os.path.basename(data['imagePath'])
            if len(data["shapes"]) == 1:
                writer.writerow({'filename': filename, 'label': 'MAMA', 'x': list(np.array(data["shapes"][0]["points"])[:,0]), 'y': list(np.array(data["shapes"][0]["points"])[:,1])})
            else:
                if data["shapes"][0]['label'] == 'MAMA':
                    mama_idx = 0
                    peitoral_idx = 1
                else:
                    mama_idx = 1
                    peitoral_idx = 0
                writer.writerow({'filename': filename, 'label': 'MAMA', 'x': list(np.array(data["shapes"][mama_idx]["points"])[:,0]), 'y': list(np.array(data["shapes"][mama_idx]["points"])[:,1])})
                writer.writerow({'filename': filename, 'label': 'PEITORAL', 'x': list(np.array(data["shapes"][peitoral_idx]["points"])[:,0]), 'y': list(np.array(data["shapes"][peitoral_idx]["points"])[:,1])})
        
        
        #csv_filename = json_filename.split('.')[0]+'.csv'
        #csv_file_path = os.path.sep.join([csv_files_path, csv_filename])
        #with open(csv_file_path, mode='w') as csv_file:
        #    writer = csv.DictWriter(csv_file, csv_headers)
        #    writer.writeheader()
        #    writer.writerow({'filename': filename, 'label': 'MAMA', 'x': list(np.array(data["shapes"][0]["points"])[:,0]), 'y': list(np.array(data["shapes"][0]["points"])[:,1])})
        #    if len(data["shapes"]) > 1:
        #        writer.writerow({'filename': filename, 'label': 'PEITORAL', 'x': list(np.array(data["shapes"][1]["points"])[:,0]), 'y': list(np.array(data["shapes"][1]["points"])[:,1])})
            

