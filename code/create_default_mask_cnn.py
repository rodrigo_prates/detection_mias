from keras_segmentation.predict import model_from_checkpoint_path
from keras_segmentation.data_utils.data_loader import *
from keras_segmentation.models.config import IMAGE_ORDERING
import config
import numpy as np
import cv2
from SegImages import SegImages
import base64
import json
import os


imgpath = config.DATASETS_PATH + '/MIAS_PNG/MIAS/images/mdb001.png'
imgname = os.path.basename(imgpath).split('.')[0]
modelname = 'mobilenet_unet_none_win224_metric_accuracy_mias.h5'
modelpath = config.MODELS_PATH + '/mobilenet_unet/checkpoints/' + modelname
model = model_from_checkpoint_path(modelpath, True)
seg = SegImages()
output_width = model.output_width
output_height  = model.output_height
input_width = model.input_width
input_height = model.input_height
n_classes = model.n_classes

labels = config.NEWLABELS
mask_points = 10

img = cv2.imread(imgpath)
height, width = img.shape[:2]

img, flipped = seg.flip_image(img, 'left')

imgwindow_arr = get_image_array(img, input_width, input_height, ordering=IMAGE_ORDERING)
pr = model.predict(np.array([imgwindow_arr]))[0]
pr = pr.reshape((output_height, output_width, n_classes)).argmax( axis=2 )

seg_img = np.zeros((output_height, output_width, 3))

for c in list(labels.values()):
    color = list(labels.keys())[list(labels.values()).index(c)]
    seg_img[:,:,0] += ( (pr[:,: ] == c )*( color[2] )).astype('uint8')
    seg_img[:,:,1] += ((pr[:,: ] == c )*( color[1] )).astype('uint8')
    seg_img[:,:,2] += ((pr[:,: ] == c )*( color[0] )).astype('uint8')

if flipped:
    seg_img = cv2.flip(seg_img, 1)

seg_img = cv2.resize(seg_img, (width, height), interpolation=cv2.INTER_NEAREST)

seg_img = seg_img.astype(np.uint8)

gray = cv2.cvtColor(seg_img, cv2.COLOR_BGR2GRAY)

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

gray = clahe.apply(gray)

edged = cv2.Canny(gray, 16, 200) 
cv2.waitKey(0)

contours,_ = cv2.findContours(edged,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)

x = contours[0][:,0][:,1]
y = contours[0][:,0][:,0]
n = len(x)

idx = np.round(np.linspace(1,n,mask_points+1))
idx = idx[:-1]
idx = idx.astype(int)
x = x[idx]
y = y[idx]

if len(contours) > 1:
    default_json = config.DIRPATH + '/default_MLO.json'
else:
    default_json = config.DIRPATH + '/default_CC.json'

with open(default_json) as f:
  data = json.load(f)
encoded = base64.b64encode(open(imgpath, "rb").read()).decode()
data['imageData'] = encoded
data['imagePath'] = imgpath

data["shapes"][0]["label"] = 'MAMA'
data["shapes"][0]["points"] = seg.labelmelist(y.tolist(),x.tolist())

if len(contours) > 1:
    x = contours[1][:,0][:,1]
    y = contours[1][:,0][:,0]
    n = len(x)

    idx = np.round(np.linspace(1,n,mask_points+1))
    idx = idx[:-1]
    idx = idx.astype(int)
    x = x[idx]
    y = y[idx]
    data["shapes"][1]["label"] = 'PEITORAL'
    data["shapes"][1]["points"] = seg.labelmelist(y.tolist(),x.tolist())

folderpath = config.DATASETS_PATH + "/MIAS_PNG/MIAS/images"
json_file_name = folderpath + '/' + imgname + ".json"
with open(json_file_name, 'w') as json_file:
    json.dump(data, json_file)
os.system("labelme " + imgpath + " -O " + json_file_name)