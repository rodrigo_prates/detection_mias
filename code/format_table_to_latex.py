import os
import pandas as pd


tab1 = "result_folds_mobilenet_unet_unet_none_none_adadelta_adadelta_accuracy_accuracy_none_none_miasInbreastDdsm_clw0_0smooth0_0denoise0_0.csv"
tab2 = "result_folds_resnet50_unet_segnet_none_none_adadelta_adadelta_accuracy_accuracy_none_none_miasInbreastDdsm_clw0_0smooth0_0denoise0_0.csv"
tab3 = "result_folds_vgg_unet_none_adadelta_accuracy_none_miasInbreastDdsm_clw0smooth0denoise0.csv"

result_path = "D:\DOUTORADO_2020\detection_mias\RESULTS\MIASINBREASTDDSM"

metrics = ["ja","di","pr","re","ac","sp","fp","fn"]
models = ["re","mo","vg","un","se"]

default_table = '''
    \\begin{table}[H]
        \caption{Métricas dos modelos com otimização na mias}
        \centering
        \\begin{tabular}{|c|c|c|c|c|c|}
            \hline
            Métricas & resnet50-unet & mobilenet-unet & vgg-unet & unet & segnet \\\\
            \hline
            Jaccard & $ja_re_val \pm ja_re_std$ & $ja_mo_val \pm ja_mo_std$ & $ja_vg_val \pm ja_vg_std$ & $ja_un_val \pm ja_un_std$ & $ja_se_val \pm ja_se_std$ \\\\
            \hline
            Dice & $di_re_val \pm di_re_std$ & $di_mo_val \pm di_mo_std$ & $di_vg_val \pm di_vg_std$ & $di_un_val \pm di_un_std$ & $di_se_val \pm di_se_std$ \\\\
            \hline
            Precision & $pr_re_val \pm pr_re_std$ & $pr_mo_val \pm pr_mo_std$ & $pr_vg_val \pm pr_vg_std$ & $pr_un_val \pm pr_un_std$ & $pr_se_val \pm pr_se_std$ \\\\
            \hline
            Recall & $re_re_val \pm re_re_std$ & $re_mo_val \pm re_mo_std$ & $re_vg_val \pm re_vg_std$ & $re_un_val \pm re_un_std$ & $re_se_val \pm re_se_std$ \\\\
            \hline
            Accuracy & $ac_re_val \pm ac_re_std$ & $ac_mo_val \pm ac_mo_std$ & $ac_vg_val \pm ac_vg_std$ & $ac_un_val \pm ac_un_std$ & $ac_se_val \pm ac_se_std$ \\\\
            \hline
            Specificity & $sp_re_val \pm sp_re_std$ & $sp_mo_val \pm sp_mo_std$ & $sp_vg_val \pm sp_vg_std$ & $sp_un_val \pm sp_un_std$ & $sp_se_val \pm sp_se_std$ \\\\
            \hline
            False Positive Rate & $fp_re_val \pm fp_re_std$ & $fp_mo_val \pm fp_mo_std$ & $fp_vg_val \pm fp_vg_std$ & $fp_un_val \pm fp_un_std$ & $fp_se_val \pm fp_se_std$ \\\\
            \hline
            False Negative Rate & $fn_re_val \pm fn_re_std$ & $fn_mo_val \pm fn_mo_std$ & $fn_vg_val \pm fn_vg_std$ & $fn_un_val \pm fn_un_std$ & $fn_se_val \pm fn_se_std$ \\\\
            \hline
        \end{tabular}
        \label{tab:metrics_after_mias}
    \end{table}
'''

#tabs = [tab1,tab2,tab3]
csvfile1 = os.path.sep.join([ result_path, tab1 ])
csvfile2 = os.path.sep.join([ result_path, tab2 ])
csvfile3 = os.path.sep.join([ result_path, tab3 ])

df1 = pd.read_csv(csvfile1)
#df1 = df1.drop([4]) #Remove IoU values
df2 = pd.read_csv(csvfile2)
#df2 = df2.drop([4])
df3 = pd.read_csv(csvfile3)
#df3 = df3.drop([4])

df = pd.concat([df1.drop(df1.columns[0], axis=1), df2.drop(df2.columns[0], axis=1), df3.drop(df3.columns[0], axis=1)], axis=1)

print(df)

for idx in range(0,len(metrics)):
    for idy in range(0,len(models)):
        val_label = metrics[idx]+'_'+models[idy]+'_val'
        std_label = metrics[idx]+'_'+models[idy]+'_std'
        default_table = default_table.replace( val_label, str(round(float(df.iloc[idx,idy].split('+/-')[0]),3)) )
        default_table = default_table.replace( std_label, str(round(float(df.iloc[idx,idy].split('+/-')[1]),3)) )

print(default_table)

