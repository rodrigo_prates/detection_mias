from SegImages import SegImages
import config


seg = SegImages()
imagePath = config.DATASETS_PATH + '/MIAS_PNG/MIAS/images/mdb001.png'
segPath = config.PREDICTIONPATH + '/MIAS/mobilenet_unet/images/predseg_mdb001.png'
outPath = 'test.png'
breast_label = (82, 82, 146)
seg.segmentBreast(imagePath, segPath, outPath, breast_label)