from SegImages import SegImages
import os
import config


maskspath = os.path.sep.join([config.DIRPATH, 'vgg_unet', 'PREDICTIONS'])
imagespath = os.path.sep.join([config.DIRPATH, 'test_input'])
outputpath = os.path.sep.join([config.DIRPATH, 'vgg_unet', 'images_segmented'])
seg = SegImages()
seg.joinImagesAndMasks(imagespath, maskspath, outputpath)