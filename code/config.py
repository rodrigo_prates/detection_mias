import os


SEGMODEL = 'mobilenet_unet'

#DIRPATH = os.getcwd()
DIRPATH = os.path.normpath(os.getcwd() + os.sep + os.pardir)
DATASETS_PATH = os.path.sep.join([DIRPATH, 'DATASETS'])
MODELS_PATH = os.path.sep.join([DIRPATH, 'models'])
RESULTS_PATH = os.path.sep.join([DIRPATH, 'RESULTS'])

INPUTPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_PNG', 'MIAS', 'images_denoise_clahe'])
#LABELPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_PNG', 'MIAS', 'labels'])
#MASKPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_PNG', 'MIAS', 'masks'])
NEWLABELPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_PNG', 'MIAS', 'newlabels'])
NEWMASKPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_PNG', 'MIAS', 'newmasks'])

DDSM_INPUTPATH = os.path.sep.join([DATASETS_PATH, 'DDSM_FULL_VIEW_PNG', 'samples'])
DDSM_TESTPATH = os.path.sep.join([DATASETS_PATH, 'DDSM_FULL_VIEW_PNG', 'tests'])
DDSM_LABELPATH = os.path.sep.join([DATASETS_PATH, 'DDSM_FULL_VIEW_PNG', 'labels'])
DDSM_MASKPATH = os.path.sep.join([DATASETS_PATH, 'DDSM_FULL_VIEW_PNG', 'masks'])

INBREAST_INPUTPATH = os.path.sep.join([DATASETS_PATH, 'INBREAST_FULL_VIEW_PNG', 'images'])
#INBREAST_TESTPATH = os.path.sep.join([DATASETS_PATH, 'INBREAST_FULL_VIEW_PNG', 'tests'])
INBREAST_LABELPATH = os.path.sep.join([DATASETS_PATH, 'INBREAST_FULL_VIEW_PNG', 'labels'])
INBREAST_MASKPATH = os.path.sep.join([DATASETS_PATH, 'INBREAST_FULL_VIEW_PNG', 'masks'])
INBREAST_ML_INPUTPATH = os.path.sep.join([DATASETS_PATH, 'INBREAST_FULL_VIEW_PNG', 'images_ml_denoise_clahe'])
INBREAST_ML_LABELPATH = os.path.sep.join([DATASETS_PATH, 'INBREAST_FULL_VIEW_PNG', 'labels_ml'])

MIAS_INBREAST_INPUTPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST', 'images_denoise_clahe'])
MIAS_INBREAST_LABELPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST', 'labels'])
MIAS_INBREAST_MASKPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST', 'masks'])

MIAS_INBREAST_DDSM_INPUTPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST_DDSM', 'images'])
MIAS_INBREAST_DDSM_LABELPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST_DDSM', 'labels'])
MIAS_INBREAST_DDSM_MASKPATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST_DDSM', 'masks'])

#CROSS_VALID_PATH = os.path.sep.join([DIRPATH, 'kfolds'])
MIAS_CROSS_VALID_PATH = os.path.sep.join([DATASETS_PATH, 'MIAS_PNG', 'MIAS', 'kfolds'])
DDSM_CROSS_VALID_PATH = os.path.sep.join([DATASETS_PATH, 'DDSM_FULL_VIEW_PNG', 'kfolds'])
INBREAST_CROSS_VALID_PATH = os.path.sep.join([DATASETS_PATH, 'INBREAST_FULL_VIEW_PNG', 'kfolds'])
MIAS_INBREAST_CROSS_VALID_PATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST', 'kfolds'])
MIAS_INBREAST_DDSM_CROSS_VALID_PATH = os.path.sep.join([DATASETS_PATH, 'MIAS_INBREAST_DDSM', 'kfolds'])

OUTPUTPATH = os.path.sep.join([MODELS_PATH, SEGMODEL])

#PREDICTIONPATH = os.path.sep.join([OUTPUTPATH, 'PREDICTIONS'])
PREDICTIONPATH = os.path.sep.join([DIRPATH, 'PREDICTIONS'])

EVALUATEPATH = os.path.sep.join([OUTPUTPATH, 'EVALUATES'])

#MASKPATH = os.path.sep.join([DIRPATH, 'MASKS'])

TRAINSIZE = 0.7

KFOLDS = 10

BATCH_SIZE = 2

NUM_LABELS = 3

NUM_EPOCHS = 100

LABELS = {(64,0,0): 0,
          (0, 64, 0): 1, 
          (82, 82, 146): 2, 
          (212, 212, 148): 3}

LABEL_NAMES = ["FUNDO_ZEROS", 
                "FUNDO_REAL", 
                "MAMA", 
                "PEITORAL"]

NEWLABELS = {(0, 64, 0): 0, 
          (82, 82, 146): 1, 
          (212, 212, 148): 2}

NEW_LABEL_NAMES = ["FUNDO_REAL", 
                "MAMA", 
                "PEITORAL"]

DO_AUGMENTATION = True
AUGMENTATION_NAME = 'aug_geometric' #aug_geometric, #aug_non_geometric, #aug_all2, #aug_all, #none
AUGMENTATION_NUM_TRIES = 4

PATIENCE = 15

USE_GPU = True

NORM_POS = 'left' #None, left, right

USE_CLASS_WEIGHTS = True

#https://www.pugetsystems.com/labs/hpc/How-to-Install-TensorFlow-with-GPU-Support-on-Windows-10-Without-Installing-CUDA-UPDATED-1419/
#https://stackoverflow.com/questions/52346957/cudagetdevice-failed-status-cuda-driver-version-is-insufficient-for-cuda-run
#https://medium.com/@coreehi/here-is-how-to-solve-ab89c2417c73
#https://medium.com/@naomi.fridman/install-conda-tensorflow-gpu-and-keras-on-ubuntu-18-04-1b403e740e25