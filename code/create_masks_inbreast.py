from SegImages import SegImages
import os
import config


seg = SegImages()
inbreast_full_view_path = "D:\\DOUTORADO_2020\\detection_mias\\INBREAST_FULL_VIEW_PNG\\images"
csvpath = "D:\\DOUTORADO_2020\\detection_mias\\segmentacao_inbreast_csv.csv"
labels = config.NEWLABELS
label_names = config.NEW_LABEL_NAMES
outputpath_masks = "D:\\DOUTORADO_2020\\detection_mias\\INBREAST_FULL_VIEW_PNG\\masks"
outputpath_labels = "D:\\DOUTORADO_2020\\detection_mias\\INBREAST_FULL_VIEW_PNG\\labels"
default_background = "FUNDO_REAL"
seg.createMasksFromCoordsV2(inbreast_full_view_path, csvpath, labels, label_names, outputpath_masks, 
                                outputpath_labels, default_background)