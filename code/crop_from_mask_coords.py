#https://www.semicolonworld.com/question/56606/numpy-opencv-2-how-do-i-crop-non-rectangular-region
#https://stackoverflow.com/questions/15341538/numpy-opencv-2-how-do-i-crop-non-rectangular-region
#SegImages.py (maskImageFromCoord, imageCropfromMaskLibra, imageCropfromMaskPanorama)
import cv2
import json
import numpy as np
import os
from tqdm import tqdm
from colorama import Fore


jsons_path = 'D:/DOUTORADO_2020/breast_density_classification/mini_ddsm/cropped_2_json'
images_path = 'D:/DOUTORADO_2020/breast_density_classification/mini_ddsm/train_path/2'
output_cropped_images = 'D:/DOUTORADO_2020/breast_density_classification/mini_ddsm/train_cropped_path/2'
output_cropped_labels = 'D:/DOUTORADO_2020/breast_density_classification/mini_ddsm/train_cropped_label_path/2'

def check_image_orientation(image):
    img_orientation = None
    if isinstance(image, str):
        img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    else:
        img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_sum = np.sum(img, axis=0)
    img_mean = int(len(img_sum)/2)
    left_sum = np.sum(img_sum[0:img_mean])
    right_sum = np.sum(img_sum[img_mean::])
    if left_sum > right_sum:
        img_orientation = 'left'
    else:
        img_orientation = 'right'
    return img_orientation

def flip_image(image, new_pos='left'):
    flipped = False
    image_pos = check_image_orientation(image)
    if image_pos != new_pos:
        image = cv2.flip(image, 1)
        flipped = True
    return image, flipped

if not os.path.isdir(output_cropped_images):
    os.makedirs(output_cropped_images)

if not os.path.isdir(output_cropped_labels):
    os.makedirs(output_cropped_labels)

for f in tqdm(os.listdir(jsons_path), desc="reading jsons coords", position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.BLUE, Fore.RESET)):
    with open( os.path.sep.join([jsons_path, f]) ) as json_file:
        data = json.load(json_file)
        coords = [tuple(coord) for coord in data["shapes"][0]["points"]]
        coords = np.array([coords], dtype=np.int32)
        image_name = f.split('json')[0]+'png'
        image = cv2.imread(os.path.sep.join([images_path, image_name]))
        mask = np.zeros(image.shape, dtype=np.uint8)
        channel_count = image.shape[2]
        ignore_mask_color = (255,)*channel_count
        cv2.fillPoly(mask, coords, ignore_mask_color)
        masked_image = cv2.bitwise_and(image, mask)
        masked_image, _ = flip_image(masked_image)
        cv2.imwrite(os.path.sep.join([output_cropped_images, image_name]), masked_image)
        masked_image[np.all(masked_image != (0,0,0), axis=-1)] = (1,1,1)
        cv2.imwrite(os.path.sep.join([output_cropped_labels, image_name]), masked_image)