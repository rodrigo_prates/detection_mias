import config
import os

if not config.USE_GPU:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages


seg = SegImages()
models = ["vgg_unet"]
custom_losses = ["none"] #dice_loss, jaccard_loss. Default 'none' for categorical_crossentropy
optimizers = ["adadelta"] # adam or adadelta
metrics = ["accuracy"] # iou, accuracy, accuracy_and_iou, dice
monitors = ["val_loss"] # val_loss, val_mean_io_u, val_mean_dice etc.
augs = ["none"] #aug_geometric, #aug_non_geometric, #aug_all2, #aug_all, #none
database = "miasInbreastDdsm" #mias, inbreast, ddsm, miasInbreast, miasInbreastDdsm
with_clw = ["0"] #0 -> False, 1 -> True (class_weights)
with_smooth = ["0"] #0 -> False, 1 -> True (class_weights)
with_denoise = ["0"] #0 -> False, 1 -> True
base_name = '_'.join(models) + '_' + '_'.join(custom_losses) + '_' + '_'.join(optimizers) + '_' + '_'.join(metrics) + '_' + '_'.join(augs) + '_' + \
            database + '_' + 'clw' + '_'.join(with_clw) + 'smooth' + '_'.join(with_smooth) + 'denoise' + '_'.join(with_denoise)

outputname = "result_folds_" + base_name + '.csv'
outputname_mama = "mama_result_folds_" + base_name + '.csv'
outputname_peitoral = "peitoral_result_folds_" + base_name + '.csv'
outputname_fundo = "fundo_result_folds_" + base_name + '.csv'
outputname_logs = 'logs_' + base_name + '.csv'
#outputname = 'models_results_mobilenet_unet_150ep_acc.csv'

#training_with_gpu = [True] #if false, set CUDA_VISIBLE_DEVICES to -1
CROSS_VALID_PATH = ''
if database == "mias":
    CROSS_VALID_PATH = config.MIAS_CROSS_VALID_PATH
elif database == "inbreast":
    CROSS_VALID_PATH = config.INBREAST_CROSS_VALID_PATH
elif database == "miasInbreast":
    CROSS_VALID_PATH = config.MIAS_INBREAST_CROSS_VALID_PATH
elif database == "miasInbreastDdsm":
    CROSS_VALID_PATH = config.MIAS_INBREAST_DDSM_CROSS_VALID_PATH
else:
    CROSS_VALID_PATH = config.MIAS_INBREAST_DDSM_CROSS_VALID_PATH

IOU_LIST, STD_LIST = seg.train_all_models_with_cross_valid(models, config.KFOLDS, CROSS_VALID_PATH, config.MODELS_PATH, 
    config.BATCH_SIZE, config.NEWLABELS, config.NEW_LABEL_NAMES, config.NUM_EPOCHS, custom_losses, optimizers, metrics, monitors, augs, 
    with_clw, with_smooth, with_denoise, outputname, outputname_mama, outputname_peitoral, outputname_fundo, outputname_logs, base_name, database)

#print(IOU_LIST)
#print(STD_LIST)