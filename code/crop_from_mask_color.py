import cv2
import numpy as np
import os
from tqdm import tqdm
from colorama import Fore


ref_image_folder = "D:/DOUTORADO_2020/breast_density_classification/inbreast/test_path/2"
orig_image_folder = "D:/DOUTORADO_2020/detection_mias/DATASETS/INBREAST_FULL_VIEW_PNG/images"
orig_mask_folder = "D:/DOUTORADO_2020/detection_mias/DATASETS/INBREAST_FULL_VIEW_PNG/masks"
output_cropped = 'D:/DOUTORADO_2020/breast_density_classification/inbreast/test_cropped_path/2'
output_cropped_label = 'D:/DOUTORADO_2020/breast_density_classification/inbreast/test_cropped_label_path/2'
breast_color = (82, 82, 146)

def check_image_orientation(image):
    img_orientation = None
    if isinstance(image, str):
        img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    else:
        img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_sum = np.sum(img, axis=0)
    img_mean = int(len(img_sum)/2)
    left_sum = np.sum(img_sum[0:img_mean])
    right_sum = np.sum(img_sum[img_mean::])
    if left_sum > right_sum:
        img_orientation = 'left'
    else:
        img_orientation = 'right'
    return img_orientation

def flip_image(image, new_pos='left'):
    flipped = False
    image_pos = check_image_orientation(image)
    if image_pos != new_pos:
        image = cv2.flip(image, 1)
        flipped = True
    return image, flipped

if not os.path.isdir(output_cropped):
    os.makedirs(output_cropped)

if not os.path.isdir(output_cropped_label):
    os.makedirs(output_cropped_label)

for image_name in tqdm(os.listdir(ref_image_folder), desc="reading images", position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.BLUE, Fore.RESET)):
    image = cv2.imread(os.path.sep.join([orig_image_folder, image_name]))
    mask = cv2.imread(os.path.sep.join([orig_mask_folder, image_name]))
    mask[np.all(mask != breast_color[::-1], axis=-1)] = (0,0,0)
    mask[np.all(mask == breast_color[::-1], axis=-1)] = (255,255,255)
    mask_out=cv2.subtract(mask,image)
    final_img=cv2.subtract(mask,mask_out)
    final_img = cv2.resize(final_img, (224, 224))
    final_img, _ = flip_image(final_img)
    cv2.imwrite(os.path.sep.join([output_cropped, image_name]), final_img)
    mask[np.all(mask == (255,255,255), axis=-1)] = (1,1,1)
    mask = cv2.resize(mask, (224, 224))
    mask, _ = flip_image(mask)
    cv2.imwrite(os.path.sep.join([output_cropped_label, image_name]), mask)