from SegImages import SegImages
import os
import config


seg = SegImages()
labelpath = config.LABELPATH
maskpath = config.MASKPATH
orig_classes = [0,1,2,3]
dest_classes = [0,0,1,2]
orig_colors = [(64,0,0), (0, 64, 0), (82, 82, 146), (212, 212, 148)]
dest_colors = [(0, 64, 0), (0, 64, 0), (82, 82, 146), (212, 212, 148)]
#labels = config.LABELS
newlabelpath = config.NEWLABELPATH
newmaskpath = config.NEWMASKPATH
seg.re_color_database(labelpath, maskpath, orig_classes, dest_classes, orig_colors, dest_colors, newlabelpath, newmaskpath)