import labelme
import os, sys
import json
import base64


default_json = 'default_json.json'

images_list = ["mdb001.png", "mdb002.png", "mdb003.png", "mdb004.png"]

with open(default_json) as f:
  data = json.load(f)

count = 0
imgpath = "DATASETS\\MIAS_PNG\\MIAS\\images"

for im in images_list:
  #print("labelme " + im + " -O " + im.split('.')[0] + ".json")
  impath = imgpath + '\\' + im
  encoded = base64.b64encode(open(impath, "rb").read()).decode()
  data['imageData'] = encoded
  data['imagePath'] = impath

  count += 1
  data["shapes"][0]["points"][0][1] += count
  json_file_name = imgpath + '\\' + im.split('.')[0] + ".json"
  with open(json_file_name, 'w') as json_file:
    json.dump(data, json_file)

  os.system("labelme " + impath + " -O " + json_file_name)