import json
import csv
import os
import numpy as np


json_file_path = 'D:/DOUTORADO_2020/detection_mias/DATASETS/MIAS_PNG/MIAS/images/mdb001.json'
csv_file_path = 'mdb001.csv'

with open(json_file_path) as json_file: 
    data = json.load(json_file)

label = data["shapes"][0]["label"]
points = data["shapes"][0]["points"]
filename = os.path.basename(data['imagePath'])
region_count = 2 if len(data["shapes"])>1 else 1

region_shape_attributes = "{""name"":""polyline"",""all_points_x"":" + str(np.array(data["shapes"][0]["points"])[:,0]) \
     + ",""all_points_y"":" + str(np.array(data["shapes"][0]["points"])[:,1]) + "}"

region_attributes = "{""PEITORAL"":"" "",""MAMA"":""MAMA""}"

csv_headers = ['filename','file_size','file_attributes','region_count','region_id','region_shape_attributes','region_attributes']

with open(csv_file_path, mode='w') as csv_file:
    writer = csv.writer(csv_file, quoting=csv.QUOTE_NONNUMERIC, delimiter=',')
    writer.writerow(csv_headers)
    writer.writerow([filename,0,"{}",region_count,0,region_shape_attributes,region_attributes])

