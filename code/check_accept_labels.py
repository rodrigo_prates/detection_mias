from SegImages import SegImages
import os
import config

seg = SegImages()

labels_path = 'D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_INBREAST\\kfolds\\train0_output'
accept_labels = list(config.NEWLABELS.values())

seg.check_label_images_out_of_bound(labels_path, accept_labels)