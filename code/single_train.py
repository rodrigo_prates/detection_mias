import config
import os

if not config.USE_GPU:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from keras_segmentation.models.unet import mobilenet_unet
from SegImages import SegImages
from tqdm import tqdm
from colorama import Fore
import math

seg = SegImages()

modelname="mobilenet_unet"
custom_loss = "none"
optimizer = "adadelta"
metric = "accuracy"
monitor = "val_loss"
augs = "none"
database = "ddsm"
with_clw = "0"
norm_pos = config.NORM_POS
numepochs = config.NUM_EPOCHS
batchsize = config.BATCH_SIZE
denoise = 0

Network = mobilenet_unet

NEWLABELS = {(0, 0, 0): 0, 
          (82, 82, 146): 1}

#numlabels = len(config.NEWLABELS)
numlabels = len(NEWLABELS)

input_height, input_width = (224, 224)

model = Network(n_classes=numlabels, input_height=input_height, input_width=input_width)

inputtrainpath = 'D:/DOUTORADO_2020/breast_density_classification/all_cropped_datasets/train_input'
outputtrainpath = 'D:/DOUTORADO_2020/breast_density_classification/all_cropped_datasets/train_output'
inputvalpath = 'D:/DOUTORADO_2020/breast_density_classification/all_cropped_datasets/val_input'
outputvalpath = 'D:/DOUTORADO_2020/breast_density_classification/all_cropped_datasets/val_output'

for file in tqdm(os.listdir(inputtrainpath), desc='Training files', position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
    imagefilepath = os.path.sep.join([inputtrainpath, file])
    labelfilepath = os.path.sep.join([outputtrainpath, file])
    seg.imgredimension(imagefilepath, input_height, input_width, norm_pos)
    seg.imgredimension(labelfilepath, input_height, input_width, norm_pos)

for file in tqdm(os.listdir(inputvalpath), desc='Validation files', position=0, leave=True, bar_format="{l_bar}%s{bar}%s{r_bar}" % (Fore.RED, Fore.RESET)):
    imagefilepath = os.path.sep.join([inputvalpath, file])
    labelfilepath = os.path.sep.join([outputvalpath, file])
    seg.imgredimension(imagefilepath, input_height, input_width, norm_pos)
    seg.imgredimension(labelfilepath, input_height, input_width, norm_pos)

num_samples = len([f for f in os.listdir(inputtrainpath) if os.path.isfile(os.path.join(inputtrainpath, f))])
steps_per_epoch = math.ceil(num_samples/batchsize)

val_num_samples = len([f for f in os.listdir(inputvalpath) if os.path.isfile(os.path.join(inputvalpath, f))])
val_steps_per_epoch = math.ceil(val_num_samples/batchsize)

MODELS_PATH = 'D:/DOUTORADO_2020/breast_density_classification/weights'

outputpath = os.path.sep.join([MODELS_PATH, modelname])
checkpoints_path = os.path.sep.join([outputpath, 'checkpoints'])
print('checkpoints_path: {}'.format(checkpoints_path))
if not os.path.exists(checkpoints_path):
    os.makedirs(checkpoints_path)
            
checkpoints_path = os.path.sep.join([checkpoints_path, modelname])
checkpoints_path = checkpoints_path + '_just_breast' + '_' + augs + '_win' + str(input_height) + \
                '_metric_' + metric + '_loss_' + custom_loss + '_denoise_' + str(denoise) + '_dataset_' + database
csv_log_filename = modelname + '_' + augs + '_win' + str(input_height) + '_metric_' + \
                metric + '_' + database + '.csv'

csv_log_path = os.path.sep.join([outputpath, 'logs'])
if not os.path.exists(csv_log_path):
    os.makedirs(csv_log_path)

csv_log_path = os.path.sep.join([csv_log_path, csv_log_filename])

class_weights = seg.calculate_class_weights(outputtrainpath, NEWLABELS) if with_clw == '1' else None

history = model.train(train_images=inputtrainpath, 
                        train_annotations=outputtrainpath, 
                        validate=True, 
                        val_images=inputvalpath, 
                        val_annotations=outputvalpath, 
                        epochs=numepochs, 
                        batch_size=batchsize, 
                        val_batch_size=batchsize,
                        steps_per_epoch=steps_per_epoch, 
                        val_steps_per_epoch=val_steps_per_epoch, 
                        verify_dataset=False, 
                        auto_resume_checkpoint=False, 
                        do_augment=False,
                        augmentation_name=augs, 
                        checkpoints_path=checkpoints_path,
                        csv_log_path=csv_log_path, 
                        metric_name=metric, 
                        optimizer_name=optimizer,
                        patience=config.PATIENCE, 
                        monitor=monitor,
                        class_weights=class_weights,
                        custom_loss=None if custom_loss == 'none' else custom_loss)