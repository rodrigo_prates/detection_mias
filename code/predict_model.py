import os
import config
#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

from SegImages import SegImages


seg = SegImages()

#inputtestpath = os.path.sep.join([config.OUTPUTPATH, 'test_input'])
#inputtestpath = config.DDSM_TESTPATH
#outputtestpath = config.DDSM_LABELPATH

inputtestpath = "D:/DOUTORADO_2020/detection_mias/DATASETS/DDSM_FULL_VIEW_PNG/images"
outputtestpath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_PNG\\MIAS\\labels"

#OUTPUTPATH = os.path.sep.join([config.DIRPATH, 'mobilenet_unet'])

modelpath = 'D:/DOUTORADO_2020/detection_mias/models/mobilenet_unet/checkpoints/mobilenet_unet_none_win224_metric_accuracy_loss_none_denoise_0_dataset_ddsm'

OUTPUT_PATH = os.path.sep.join([config.DIRPATH, 'PREDICTIONS', 'DDSM'])
#OUTPUT_PATH = os.path.sep.join([config.DIRPATH, 'PREDICTIONS', 'MIAS'])
#OUTPUT_PATH = os.path.sep.join([config.DIRPATH, 'PREDICTIONS', 'INBREAST'])
#OUTPUT_PATH = os.path.sep.join([config.DIRPATH, 'PREDICTIONS', 'MIAS_INBREAST'])

smooth = '1'
#thres = 0.95

#orig_dims = (1024,1024)

norm_pos = 'left'
seg.seg_model_eval(inputtestpath, modelpath, config.NEWLABELS, OUTPUT_PATH, True, smooth, norm_pos)
#seg.seg_evaluate(inputtestpath, outputtestpath, modelpath, config.NEWLABELS, OUTPUT_PATH, config.NEW_LABEL_NAMES, smooth, norm_pos)