import config
import os
from SegImages import SegImages


seg = SegImages()
datasets = ['MIAS','INBREAST','MIAS_INBREAST','MIAS_INBREAST']
#datasets = ['MIAS','INBREAST']
images = ['DATASETS\\MIAS_PNG\\MIAS\\images\\mdb015.png', 
            'DATASETS\\INBREAST_FULL_VIEW_PNG\\images_ml\\20587664_f4b2d377f43ba0bd_MG_R_ML_ANON.png',
            'DATASETS\\MIAS_INBREAST\\images\\mdb015.png',
            'DATASETS\\MIAS_INBREAST\\images\\20587664_f4b2d377f43ba0bd_MG_R_ML_ANON.png']
#images = {'MIAS': 'DATASETS\\MIAS_PNG\\MIAS\\images\\mdb015.png', 
#            'INBREAST': 'DATASETS\\INBREAST_FULL_VIEW_PNG\\images_ml\\20587664_f4b2d377f43ba0bd_MG_R_ML_ANON.png'}
models = ['resnet50_unet', 'mobilenet_unet', 'vgg_unet', 'unet', 'segnet', 'mlp']
model_nicks = ['Resnet50_Unet', 'Mobilenet_Unet', 'Vgg_Unet', 'Unet', 'Segnet', 'MLP']
#models = ['resnet50_unet', 'mobilenet_unet', 'vgg_unet', 'unet', 'segnet']
seg.multi_plot_predictions(datasets, models, images, model_nicks)