import os
import config
from SegImages import SegImages


seg = SegImages()

imagesPath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_INBREAST\\images"

OUTPUTPATH = os.path.sep.join([config.MODELS_PATH, 'segnet'])

modelpath = os.path.sep.join([OUTPUTPATH, 'checkpoints', 'segnet_none_win256_metric_accuracy_miasInbreast.h5'])

smooth = '1'

norm_pos = 'left'

OUTPUT_PATH = "D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\\MIAS_INBREAST\\segnet\\images"

seg.seg_model_eval(imagesPath, modelpath, config.NEWLABELS, OUTPUT_PATH, True, smooth, norm_pos)

labelDict = {0:(0, 64, 0), 1: (82, 82, 146), 2: (212, 212, 148)}

outPath = "D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\\MIAS_INBREAST\\segnet\\contours"

resize_to = 1024 #Number or None
is_prediction=True
seg.segmentByContourv2(imagesPath, OUTPUT_PATH, labelDict, outPath, resize_to, is_prediction)
