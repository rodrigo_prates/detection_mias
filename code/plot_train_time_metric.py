import pandas as pd
import numpy as np
from SegImages import SegImages
import os


#f1 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_resnet50_unet_mobilenet_unet_none_none_adadelta_adadelta_accuracy_accuracy_none_none_mias_clw0_0smooth0_0.csv"
#f2 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_vgg_unet_unet_none_none_adadelta_adadelta_accuracy_accuracy_none_none_mias_clw0_0smooth0_0.csv"
#f3 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\histories_segnet_none_adadelta_accuracy_none_mias_clw0smooth0.csv"

f11 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\train_time_resnet50_mobilenet_vgg_unet_segnet_before_optimization_miasinbreast.csv"
f12 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_before_optimization_miasinbreast.csv"

f21 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\train_time_resnet50_mobilenet_vgg_unet_segnet_after_optimization_miasinbreast.csv"
f22 = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_after_optimization_miasinbreast.csv"

seg = SegImages()

c = ['Resnet50_Unet', 'Mobilenet_Unet', 'Vgg_Unet', 'Unet', 'Segnet']

df11 = pd.read_csv(f11)
models_training_time_before = df11.iloc[0,1:].values.T

df12 = pd.read_csv(f12)
models_training_jaccard_before = df12.mean(axis=0).values.T

df21 = pd.read_csv(f21)
models_training_time_after = df21.iloc[0,1:].values.T

df22 = pd.read_csv(f22)
models_training_jaccard_after = df22.mean(axis=0).values.T

metric_name = 'Jaccard'

plotfilepath='D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\train_time_resnet50_mobilenet_vgg_unet_segnet_miasinbreastv3.png' 
seg.plottrainingtime2(models_training_time_before, models_training_time_after, models_training_jaccard_before, 
                        models_training_jaccard_after, c, metric_name, plotfilepath)