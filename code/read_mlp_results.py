import os
import scipy.io
import numpy as np

""" IoU - Jaccard
F1s - Dice
PPV - Precision
TPR - Recall
ACC - Accuracy
TNR - Specificity
FPR - False positive rate
FNR - False negative rate """

#mat_file = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIAS_MLP.mat"
mat_file = "D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\BOTH_MLP.mat"
metrics = ['IoU', "F1s", "PPV", "TPR", "ACC", "TNR", "FPR", "FNR"]
#metric = 'IoU'

#cross_valid_intervals_str = "1-32, 33-64, 65-97, 98-129, 130-161, 162-193, 194-225, 226-258, 259-290, 291-322"
#cross_valid_intervals = [range(0,32), range(32,64), range(64,97), range(97,129), range(129,161), range(161,193), 
#                        range(193,225), range(225,258), range(258,290), range(290,322)]

cross_valid_intervals = [range(0,20), range(20,40), range(40,60), range(60,80), range(80,100), range(100,120), 
                        range(120,140), range(140,160), range(160,180), range(180,201)]
mat_dict = scipy.io.loadmat(mat_file)

for metric in metrics:
    metric_dict = mat_dict[metric]
    metric_best_model = metric_dict[4,4]

    metric_mean = np.mean(np.mean(metric_best_model, axis=0))
    metric_std = np.std(np.std(metric_best_model, axis=0))
    print('mean {}: {}'.format(metric, metric_mean))
    print('std {}: {}'.format(metric, metric_std))

    metric_classes_mean = np.mean(metric_best_model, axis=1)
    metric_folds_mean = []
    for inter in cross_valid_intervals:
        metric_folds_mean.append(np.mean(metric_classes_mean[inter]))
    print('mean {} for each fold: {}'.format(metric, metric_folds_mean))

    metric_bkg = metric_best_model[0,:]
    metric_chest = metric_best_model[1,:]
    metric_pectoral = metric_best_model[2,:]

    metric_bkg_mean = np.mean(metric_bkg)
    metric_bkg_std = np.std(metric_bkg)
    print('mean bkg {}: {}'.format(metric, metric_bkg_mean))
    print('std bkg {}: {}'.format(metric, metric_bkg_std))

    metric_chest_mean = np.mean(metric_chest)
    metric_chest_std = np.std(metric_chest)
    print('mean chest {}: {}'.format(metric, metric_chest_mean))
    print('std chest {}: {}'.format(metric, metric_chest_std))

    metric_pectoral_mean = np.mean(metric_pectoral)
    metric_pectoral_std = np.std(metric_pectoral)
    print('mean pectoral {}: {}'.format(metric, metric_pectoral_mean))
    print('std pectoral {}: {}'.format(metric, metric_pectoral_std))

    print("*******************************************************************")
