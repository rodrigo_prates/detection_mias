import cv2
import numpy as np
import os
from matplotlib import pyplot as plt


#image_path = 'C:\\Users\\rodri\\Downloads\\Imagens_teste-20200812T013022Z-001\\Imagens_teste\\2-ANP-2A-RJS_T2_CX5_19.PNG'
image_path = 'D:\\DOUTORADO_2020\\detection_mias\\deblur_DSC_2163.JPG'
#image_path = 'C:\\Users\\rodri\\Downloads\\3-RJS-744-RJ_TESTO01_CX8_70\\DSC_2163.JPG'
img = cv2.imread(image_path)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
winsize = 56
winstep = 56
#blur_limit = 2900

filename = os.path.basename(image_path)

height, width = gray.shape[:2]
blur_img = np.ones( (round(height/winsize), round(width/winsize)) )*-1
count_v = 0
count_h = 0
for vindex in range(0, height - winsize, winstep):
    vert = height - winsize
    print("Image windowing process: {}%".format(round((100/(vert))*vindex,2)))
    count_h = 0
    for hindex in range(0, width - winsize, winstep):
        graywindow = gray[vindex:vindex + winsize, hindex:hindex + winsize]
        blur_img[count_v, count_h] = cv2.Laplacian(graywindow, cv2.CV_64F).var()
        count_h += 1
    count_v += 1

mask = img>-1
blur_img = blur_img[np.ix_(blur_img.any(1),blur_img.any(0))]
histogram, bin_edges = np.histogram(blur_img)

#print(np.unique(blur_img))
#print(len(np.unique(blur_img)))

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
K = 5
Z = blur_img.reshape((-1,1))
Z = np.float32(Z)
ret,label,center=cv2.kmeans(Z,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)
center = np.uint8(center)
res = center[label.flatten()]
res2 = res.reshape((blur_img.shape))

#print(np.unique(res2))
#print(len(np.unique(res2)))

#blur_img *= 255.0/blur_img.max()

plt.figure()
plt.plot(bin_edges[0:-1], histogram)
plt.savefig('blur_hist_' + filename)
cv2.imwrite('blur_' + filename, res2)