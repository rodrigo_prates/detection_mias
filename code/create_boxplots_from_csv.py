from SegImages import SegImages
import os
import config


seg = SegImages()
title="Segmentation Performance"

csvfile1="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\jaccard_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.csv"
savepath1="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\jaccard_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.png"
metric1="jaccard"
#seg.plotBoxPlot(csvfile1, savepath1, metric1, title)

csvfile2="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\dice_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.csv"
savepath2="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\dice_resnet50_mobilenet_vgg_unet_segnet_before_optimization_mias.png"
metric2="dice"
#seg.plotBoxPlot(csvfile2, savepath2, metric2, title)

#savepath="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\box_plot_resnet50_mobilenet_vgg_unet_segnet_after_optimization_inbreast.png"
#seg.joinBoxPlots(csvfile1, csvfile2, savepath, metric1, metric2)

csvfile1="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_before_optimization_miasinbreast.csv"

csvfile2="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\jaccard_resnet50_mobilenet_vgg_unet_segnet_after_optimization_miasinbreast.csv"

csvfile3="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\dice_resnet50_mobilenet_vgg_unet_segnet_before_optimization_miasinbreast.csv"

csvfile4="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\dice_resnet50_mobilenet_vgg_unet_segnet_after_optimization_miasinbreast.csv"

savepath="D:\\DOUTORADO_2020\\detection_mias\\RESULTS\\MIASINBREAST\\box_plot_resnet50_mobilenet_vgg_unet_segnet_miasinbreast.png"
seg.joinBoxPlots2(csvfile1, csvfile2, csvfile3, csvfile4, savepath, metric1, metric2)
