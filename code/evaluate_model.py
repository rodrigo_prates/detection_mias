from SegImages import SegImages
import os
import config

#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

seg = SegImages()

#inputtestpath = os.path.sep.join([config.DIRPATH, 'test_input'])
#inputtestpath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_PNG\\MIAS\\kfolds\\test0_input"
inputtestpath = "D:/DOUTORADO_2020/breast_density_classification/all_cropped_datasets/test_input"
#outputtestpath = os.path.sep.join([config.DIRPATH, 'test_output'])
#outputtestpath = "D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_PNG\\MIAS\\kfolds\\test0_output"
outputtestpath = "D:/DOUTORADO_2020/breast_density_classification/all_cropped_datasets/test_output"
#modelpath = os.path.sep.join([config.OUTPUTPATH, 'checkpoints', config.SEGMODEL])
modelpath = "D:/DOUTORADO_2020/breast_density_classification/weights/mobilenet_unet/checkpoints/mobilenet_unet_just_breast_none_win224_metric_accuracy_loss_none_denoise_0_dataset_ddsm"

smooth = '0'

#seg.seg_evaluate(inputtestpath, outputtestpath, modelpath, config.NEWLABELS, config.EVALUATEPATH, config.LABEL_NAMES, smooth)
NEWLABELS = {(0, 0, 0): 0, 
          (82, 82, 146): 1}
LABEL_NAMES = ["FUNDO_REAL", 
                "MAMA"]
EVALUATEPATH = 'D:/DOUTORADO_2020/breast_density_classification/evaluate'
seg.seg_evaluate(inputtestpath, outputtestpath, modelpath, NEWLABELS, EVALUATEPATH, LABEL_NAMES, smooth)