from SegImages import SegImages
import os
import config

#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

seg = SegImages()

inputtrainpath = os.path.sep.join([config.OUTPUTPATH, 'train_input'])
inputvalpath = os.path.sep.join([config.OUTPUTPATH, 'val_input'])
outputtrainpath = os.path.sep.join([config.OUTPUTPATH, 'train_output'])
outputvalpath = os.path.sep.join([config.OUTPUTPATH, 'val_output'])

seg.seg_model_train(inputtrainpath, inputvalpath, outputtrainpath, outputvalpath, config.OUTPUTPATH, 
                  config.SEGMODEL, config.BATCH_SIZE, config.NUM_LABELS, config.NUM_EPOCHS)