import os
import scipy.io
import numpy as np
import cv2
import config

mias_image_path = "D:/DOUTORADO_2020/detection_mias/RESULTS/mdb015.mat"
mias_label_outpath = "D:/DOUTORADO_2020/detection_mias/PREDICTIONS/MIAS/mlp/labels/predseg_mlp_mdb015.png"
mias_mask_outpath = "D:/DOUTORADO_2020/detection_mias/PREDICTIONS/MIAS/mlp/images/predseg_mdb015.png"

inbreast_image_path = "D:/DOUTORADO_2020/detection_mias/RESULTS/20587664_f4b2d377f43ba0bd_MG_R_ML_ANON.mat"
inbreast_label_outpath = "D:/DOUTORADO_2020/detection_mias/PREDICTIONS/INBREAST/mlp/labels/predseg_20587664_f4b2d377f43ba0bd_MG_R_ML_ANON.png"
inbreast_mask_outpath = "D:/DOUTORADO_2020/detection_mias/PREDICTIONS/INBREAST/mlp/images/predseg_20587664_f4b2d377f43ba0bd_MG_R_ML_ANON.png"

dict_mat_mias = scipy.io.loadmat(mias_image_path)
dict_mat_inbreast = scipy.io.loadmat(inbreast_image_path)

label_mat_mias = dict_mat_mias['Yp']
label_mat_inbreast = dict_mat_inbreast['Yp']

cv2.imwrite(mias_label_outpath, label_mat_mias)
cv2.imwrite(inbreast_label_outpath, label_mat_inbreast)

image_mat_mias = cv2.cvtColor(label_mat_mias,cv2.COLOR_GRAY2RGB)
image_mat_inbreast = cv2.cvtColor(label_mat_inbreast,cv2.COLOR_GRAY2RGB)

mlp_labels = {(0, 64, 0): 1, 
          (82, 82, 146): 2, 
          (212, 212, 148): 3}

for color in mlp_labels:
    idx = mlp_labels[color]
    image_mat_mias[np.all(image_mat_mias == (idx, idx, idx), axis=-1)] = color[::-1]
    image_mat_inbreast[np.all(image_mat_inbreast == (idx, idx, idx), axis=-1)] = color[::-1]

cv2.imwrite(mias_mask_outpath, image_mat_mias)
cv2.imwrite(inbreast_mask_outpath, image_mat_inbreast)
