#https://stackoverflow.com/questions/57576686/how-to-overlay-segmented-image-on-top-of-main-image-in-python
#https://www.geeksforgeeks.org/find-and-draw-contours-using-opencv-python/

import numpy as np
import cv2

# Load images as greyscale but make main RGB so we can annotate in colour
seg  = cv2.imread('D:\\DOUTORADO_2020\\detection_mias\\PREDICTIONS\\MIAS\\mobilenet_unet\\predseg_mdb158.png',cv2.IMREAD_GRAYSCALE)
main = cv2.imread('D:\\DOUTORADO_2020\\detection_mias\\DATASETS\\MIAS_PNG\\MIAS\\kfolds\\test0_input\\mdb158.png',cv2.IMREAD_GRAYSCALE)
main = cv2.cvtColor(main,cv2.COLOR_GRAY2BGR)

# Find Canny edges 
edged = cv2.Canny(seg, 30, 200) 
cv2.waitKey(0)

# Dictionary giving RGB colour for label (segment label) - label 1 in red, label 2 in yellow
RGBforLabel = {0:(0, 64, 0), 1: (82, 82, 146), 2: (212, 212, 148)}

# Find external contours
contours,_ = cv2.findContours(edged,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

print(len(contours))

# Iterate over all contours
for i,c in enumerate(contours):
    # Find mean colour inside this contour by doing a masked mean
    mask = np.zeros(seg.shape, np.uint8)
    print(c)
    cv2.drawContours(mask,c,-1,255, -1)
    # DEBUG: cv2.imwrite(f"mask-{i}.png",mask)
    mean,_,_,_ = cv2.mean(seg, mask=mask)
    print(f"i: {i}, mean: {mean}")
    # DEBUG: print(f"i: {i}, mean: {mean}")

    # Get appropriate colour for this label
    label = 2 if mean > 100.0 else 1
    colour = RGBforLabel.get(label)
    # DEBUG: print(f"Colour: {colour}")

    # Outline contour in that colour on main image, line thickness=1
    cv2.drawContours(main,c,-1,colour[::-1],2,cv2.LINE_AA)

# Save result
cv2.imwrite('contour.png',main) 