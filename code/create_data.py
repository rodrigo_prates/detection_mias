from SegImages import SegImages
import os
import config

seg = SegImages()

if not os.path.exists(config.OUTPUTPATH):
    os.mkdir(config.OUTPUTPATH)

seg.split_data_from_folder(config.INPUTPATH, config.LABELPATH, config.OUTPUTPATH)