from SegImages import SegImages
import os
import config

seg = SegImages()

CROSS_VALID_PATH = config.MIAS_INBREAST_DDSM_CROSS_VALID_PATH

if not os.path.exists(CROSS_VALID_PATH):
    os.mkdir(CROSS_VALID_PATH)

inputpath = config.MIAS_INBREAST_DDSM_INPUTPATH
labelpath = config.MIAS_INBREAST_DDSM_LABELPATH
outputpath = CROSS_VALID_PATH
trainsize = config.TRAINSIZE
kfolds = config.KFOLDS
norm_pos = config.NORM_POS
is_mias = False
do_augmentation = False
do_denoise=False
#aug_type = "aug_all2"
seg.split_data_cross_validation(inputpath, labelpath, outputpath, trainsize, kfolds, norm_pos, is_mias, do_augmentation, do_denoise)