# CNN FOR MAMOGRAPHY IMAGES SEGMENTATION #

This project aims to develop a CAD to detect suspicious regions in mammography exam images.
The project is divided into two stages:
1.  Segmentation of anatomical regions of mammography.
2.  Segmentation of abnormalities in the breast.

This CAD is being developed using techniques of convolutional neural networks.

The following models are being used for each stage.

| model_name             | Base Model             | Segmentation Model     | Stage                   |
|------------------------|------------------------|------------------------|------------------------ |
| mobilenet_unet         | MobileNetV1            | UNet                   | 1                       |

You can download a zip file with all models weights [best_models.zip](best_models.zip).

## INSTALLATION ##

- Python 3+ (Ex: From Anaconda)
- Create Virtual Env: conda create --name tf_gpu tensorflow-gpu
- pip install -r requirements.txt
- fork for keras_segmentation: https://bitbucket.org/rodrigo_prates/keras_segmentation/src/master/
    - Clone
    - Install with python setup.py install

## DATASET ##

## HOW TO USE ##

A base class and scripts were created for data generation, training and model evaluation. There is also a configuration file, config.py, which can be edited as needed.

- [Segimages.py](/SegImages.py): Base class with all methods for training and evaluate models for mamography segmentation.
- [train_eval_models_cv.py](/train_eval_models_cv.py): ...

## RESULTS ##

### Example ###
![Example](PREDICTIONS\MIAS\mobilenet_unet\predseg_mdb010.png)
